<?php
App::uses('Information', 'Model');

/**
 * Information Test Case
 *
 */
class InformationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.information'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Information = ClassRegistry::init('Information');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Information);

		parent::tearDown();
	}

}
