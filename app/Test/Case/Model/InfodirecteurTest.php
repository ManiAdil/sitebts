<?php
App::uses('Infodirecteur', 'Model');

/**
 * Infodirecteur Test Case
 *
 */
class InfodirecteurTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.infodirecteur'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Infodirecteur = ClassRegistry::init('Infodirecteur');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Infodirecteur);

		parent::tearDown();
	}

}
