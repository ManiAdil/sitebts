<?php
App::uses('Sitesami', 'Model');

/**
 * Sitesami Test Case
 *
 */
class SitesamiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sitesami'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sitesami = ClassRegistry::init('Sitesami');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sitesami);

		parent::tearDown();
	}

}
