<?php
App::uses('Coordone', 'Model');

/**
 * Coordone Test Case
 *
 */
class CoordoneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordone'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Coordone = ClassRegistry::init('Coordone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Coordone);

		parent::tearDown();
	}

}
