<?php
/**
 * CoordoneFixture
 *
 */
class CoordoneFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'Id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'Tel' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Fix' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Fax' => array('type' => 'integer', 'null' => false, 'default' => null),
		'Adresse' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'Id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'Id' => 1,
			'Tel' => 1,
			'Fix' => 1,
			'Fax' => 1,
			'Adresse' => 'Lorem ipsum dolor sit amet',
			'Email' => 'Lorem ipsum dolor sit amet'
		),
	);

}
