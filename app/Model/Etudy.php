<?php
App::uses('AppModel', 'Model');
/**
 * Etudy Model
 *
 * @property Matiere $Matiere
 * @property Classee $Classee
 */
class Etudy extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'matiere_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'classee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Matiere' => array(
			'className' => 'Matiere',
			'foreignKey' => 'matiere_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Classee' => array(
			'className' => 'Classee',
			'foreignKey' => 'classee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
