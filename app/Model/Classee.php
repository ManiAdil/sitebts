<?php
App::uses('AppModel', 'Model');
/**
 * Classee Model
 *
 * @property Filiere $Filiere
 * @property Niveau $Niveau
 */
class Classee extends AppModel {
   public $displayField="nom";

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nom' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'filiere_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'niveau_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Filiere' => array(
			'className' => 'Filiere',
			'foreignKey' => 'filiere_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Niveau' => array(
			'className' => 'Niveau',
			'foreignKey' => 'niveau_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
