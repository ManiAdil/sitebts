<?php
App::uses('AppModel', 'Model');
/**
 * Matiere Model
 *
 * @property Contient $Contient
 * @property Encadre $Encadre
 * @property Etudy $Etudy
 */
class Matiere extends AppModel {
    public $displayField="nom";

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'coeff' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Contient' => array(
			'className' => 'Contient',
			'foreignKey' => 'matiere_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Encadre' => array(
			'className' => 'Encadre',
			'foreignKey' => 'matiere_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Etudy' => array(
			'className' => 'Etudy',
			'foreignKey' => 'matiere_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
