<?php
App::uses('AppModel', 'Model');
/**
 * Affectation Model
 *
 * @property Classee $Classee
 * @property Enseignant $Enseignant
 */
class Affectation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'classee_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'enseignant_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'datedebut' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'datefin' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Classee' => array(
			'className' => 'Classee',
			'foreignKey' => 'classee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Enseignant' => array(
			'className' => 'Enseignant',
			'foreignKey' => 'enseignant_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
