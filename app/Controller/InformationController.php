<?php
App::uses('AppController', 'Controller');
/**
 * Information Controller
 *
 * @property Information $Information
 */
class InformationController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Information->recursive = 0;
		$this->set('information', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Information->exists($id)) {
			throw new NotFoundException(__('Invalid information'));
		}
		$options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
		$this->set('information', $this->Information->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Information->create();
			if ($this->Information->save($this->request->data)) {
				$this->Session->setFlash(__('The information has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The information could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Information->exists($id)) {
			throw new NotFoundException(__('Invalid information'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Information->save($this->request->data)) {
				$this->Session->setFlash(__('The information has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The information could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
			$this->request->data = $this->Information->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Information->id = $id;
		if (!$this->Information->exists()) {
			throw new NotFoundException(__('Invalid information'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Information->delete()) {
			$this->Session->setFlash(__('Information deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Information was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	  $this->layout="admin";
		$this->Information->recursive = 0;
		$this->set('information', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	      $this->layout="admin";
		if (!$this->Information->exists($id)) {
			throw new NotFoundException(__('Invalid information'));
		}
		$options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
		$this->set('information', $this->Information->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	   $this->layout="admin";
		if ($this->request->is('post')) {
			$this->Information->create();
			if ($this->Information->save($this->request->data)) {
				$this->Session->setFlash(__('The information has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The information could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	     $this->layout="admin";
		if (!$this->Information->exists($id)) {
			throw new NotFoundException(__('Invalid information'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Information->save($this->request->data)) {
				$this->Session->setFlash(__('The information has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The information could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
			$this->request->data = $this->Information->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	      $this->layout="admin";
		$this->Information->id = $id;
		if (!$this->Information->exists()) {
			throw new NotFoundException(__('Invalid information'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Information->delete()) {
			$this->Session->setFlash(__('Information deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Information was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
