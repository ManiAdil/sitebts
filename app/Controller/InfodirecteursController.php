<?php
App::uses('AppController', 'Controller');
/**
 * Infodirecteurs Controller
 *
 * @property Infodirecteur $Infodirecteur
 */
class InfodirecteursController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Infodirecteur->recursive = 0;
		$this->set('infodirecteurs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Infodirecteur->exists($id)) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		$options = array('conditions' => array('Infodirecteur.' . $this->Infodirecteur->primaryKey => $id));
		$this->set('infodirecteur', $this->Infodirecteur->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Infodirecteur->create();
			if ($this->Infodirecteur->save($this->request->data)) {
				$this->flash(__('Infodirecteur saved.'), array('action' => 'index'));
			} else {
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Infodirecteur->exists($id)) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Infodirecteur->save($this->request->data)) {
				$this->flash(__('The infodirecteur has been saved.'), array('action' => 'index'));
			} else {
			}
		} else {
			$options = array('conditions' => array('Infodirecteur.' . $this->Infodirecteur->primaryKey => $id));
			$this->request->data = $this->Infodirecteur->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Infodirecteur->id = $id;
		if (!$this->Infodirecteur->exists()) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Infodirecteur->delete()) {
			$this->flash(__('Infodirecteur deleted'), array('action' => 'index'));
		}
		$this->flash(__('Infodirecteur was not deleted'), array('action' => 'index'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	   $this->layout="admin";
		$this->Infodirecteur->recursive = 0;
		$this->set('infodirecteurs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	   $this->layout="admin";
		if (!$this->Infodirecteur->exists($id)) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		$options = array('conditions' => array('Infodirecteur.' . $this->Infodirecteur->primaryKey => $id));
		$this->set('infodirecteur', $this->Infodirecteur->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	   $this->layout="admin";
		if ($this->request->is('post')) {
			$this->Infodirecteur->create();
			if ($this->Infodirecteur->save($this->request->data)) {
				$this->flash(__('Infodirecteur saved.'), array('action' => 'index'));
			} else {
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	   $this->layout="admin";
		if (!$this->Infodirecteur->exists($id)) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Infodirecteur->save($this->request->data)) {
				$this->flash(__('The infodirecteur has been saved.'), array('action' => 'index'));
			} else {
			}
		} else {
			$options = array('conditions' => array('Infodirecteur.' . $this->Infodirecteur->primaryKey => $id));
			$this->request->data = $this->Infodirecteur->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	   $this->layout="admin";
		$this->Infodirecteur->id = $id;
		if (!$this->Infodirecteur->exists()) {
			throw new NotFoundException(__('Invalid infodirecteur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Infodirecteur->delete()) {
			$this->flash(__('Infodirecteur deleted'), array('action' => 'index'));
		}
		$this->flash(__('Infodirecteur was not deleted'), array('action' => 'index'));
		$this->redirect(array('action' => 'index'));
	}
}
