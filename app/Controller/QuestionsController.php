 <?php
App::uses('AppController', 'Controller');
/**
 * Questions Controller
 *
 * @property Question $Question
 */
class QuestionsController extends AppController {

public function admin_afficherquestion($id = null)  { 
    $this->layout="admin" ;
   $this->set('rep2',$this->Question->query("select count(*) as nbr,reponse_id,user_id from likes where reponse_id = ANY ( select id from reponses where question_id = ".$id.") group by reponse_id,user_id "));
   $this->set('rep',$this->Question->find("all",array("conditions"=>array("Question.id"=>$id))));
}

    public function admin_mesquestions() { $this->layout="admin" ;
  
         $this->set('mesquestions',$this->Question->find("threaded",array('conditions'=>array("Question.user_id"=>$this->Session->read('id')))));

}


	public function admin_marquer_resolu($id) {
	$this->Question->id = $id;
		$this->Question->save(array("Question"=>array("resolu"=>1)));
        $this->redirect(array("action"=>"mesquestions"));
	
	}
    public function admin_validerquestion()
    {
    if($this->Session->check('id')==true)
    {   $this->layout = "admin";
        	$this->Question->recursive = 0;
		$this->set('questions', $this->paginate());
		}else{
		 $this->redirect(array("controller"=>"Users","action"=>"login","admin"=>false));
}
    }
	public function admin_marquer_valide($id) {
	       if($this->Session->check('id')==true)
{	$this->Question->id = $id;
		$this->Question->save(array("Question"=>array("valider"=>1)));
        $this->redirect(array("action"=>"validerquestion"));
		}else{
		 $this->redirect(array("controller"=>"Users","action"=>"login","admin"=>false));
}
	
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Question->create();
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$this->request->data = $this->Question->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Question->delete()) {
			$this->Session->setFlash(__('Question deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Question was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() { $this->layout="admin" ;
        if($this->Session->check('id')==true)
        {
       	$this->Question->recursive = 0;
		$this->set('questions', $this->paginate());  
		}
        else
        {
		 $this->redirect(array("controller"=>"Users","action"=>"login","admin"=>false));
        }
	
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null){ $this->layout="admin" ;
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() { $this->layout="admin" ;
        if($this->Session->check('id')==true)
        {
            	if ($this->request->is('post')) { 
		
			$this->Question->create();
		
			if ($this->Question->save($this->request->data)) { 
				$this->Session->setFlash(__('The question has been saved'));
				$this->Question->resolu=0;
                 
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
					
			}
		}
		}
        else
        {
		 $this->redirect(array("controller"=>"Users","action"=>"login","admin"=>false));
        }
	
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) { $this->layout="admin" ;
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$this->request->data = $this->Question->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) { $this->layout="admin" ;
        if($this->Session->check('id')==true)
        {
           		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Question->delete()) {
			$this->Session->setFlash(__('Question deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Question was not deleted'));
		$this->redirect(array('action' => 'index'));
	} 
		
        else
        {
		 $this->redirect(array("controller"=>"Users","action"=>"login","admin"=>false));
        }
}
}
