<?php
App::uses('AppController', 'Controller');
/**
 * Etudies Controller
 *
 * @property Etudy $Etudy
 */
class EtudiesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Etudy->recursive = 0;
		$this->set('etudies', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Etudy->exists($id)) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		$options = array('conditions' => array('Etudy.' . $this->Etudy->primaryKey => $id));
		$this->set('etudy', $this->Etudy->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Etudy->create();
			if ($this->Etudy->save($this->request->data)) {
				$this->Session->setFlash(__('The etudy has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudy could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->Etudy->Matiere->find('list');
		$classees = $this->Etudy->Classee->find('list');
		$this->set(compact('matieres', 'classees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Etudy->exists($id)) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Etudy->save($this->request->data)) {
				$this->Session->setFlash(__('The etudy has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudy could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Etudy.' . $this->Etudy->primaryKey => $id));
			$this->request->data = $this->Etudy->find('first', $options);
		}
		$matieres = $this->Etudy->Matiere->find('list');
		$classees = $this->Etudy->Classee->find('list');
		$this->set(compact('matieres', 'classees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Etudy->id = $id;
		if (!$this->Etudy->exists()) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Etudy->delete()) {
			$this->Session->setFlash(__('Etudy deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Etudy was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Etudy->recursive = 0;
		$this->set('etudies', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Etudy->exists($id)) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		$options = array('conditions' => array('Etudy.' . $this->Etudy->primaryKey => $id));
		$this->set('etudy', $this->Etudy->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Etudy->create();
			if ($this->Etudy->save($this->request->data)) {
				$this->Session->setFlash(__('The etudy has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudy could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->Etudy->Matiere->find('list');
		$classees = $this->Etudy->Classee->find('list');
		$this->set(compact('matieres', 'classees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Etudy->exists($id)) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Etudy->save($this->request->data)) {
				$this->Session->setFlash(__('The etudy has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudy could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Etudy.' . $this->Etudy->primaryKey => $id));
			$this->request->data = $this->Etudy->find('first', $options);
		}
		$matieres = $this->Etudy->Matiere->find('list');
		$classees = $this->Etudy->Classee->find('list');
		$this->set(compact('matieres', 'classees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Etudy->id = $id;
		if (!$this->Etudy->exists()) {
			throw new NotFoundException(__('Invalid etudy'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Etudy->delete()) {
			$this->Session->setFlash(__('Etudy deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Etudy was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
