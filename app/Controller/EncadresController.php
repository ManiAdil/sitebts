<?php
App::uses('AppController', 'Controller');
/**
 * Encadres Controller
 *
 * @property Encadre $Encadre
 */
class EncadresController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Encadre->recursive = 0;
		$this->set('encadres', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Encadre->exists($id)) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		$options = array('conditions' => array('Encadre.' . $this->Encadre->primaryKey => $id));
		$this->set('encadre', $this->Encadre->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Encadre->create();
			if ($this->Encadre->save($this->request->data)) {
				$this->Session->setFlash(__('The encadre has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The encadre could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->Encadre->Matiere->find('list');
		$enseignants = $this->Encadre->Enseignant->find('list');
		$classees = $this->Encadre->Classee->find('list');
		$this->set(compact('matieres', 'enseignants', 'classees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Encadre->exists($id)) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Encadre->save($this->request->data)) {
				$this->Session->setFlash(__('The encadre has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The encadre could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Encadre.' . $this->Encadre->primaryKey => $id));
			$this->request->data = $this->Encadre->find('first', $options);
		}
		$matieres = $this->Encadre->Matiere->find('list');
		$enseignants = $this->Encadre->Enseignant->find('list');
		$classees = $this->Encadre->Classee->find('list');
		$this->set(compact('matieres', 'enseignants', 'classees'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Encadre->id = $id;
		if (!$this->Encadre->exists()) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Encadre->delete()) {
			$this->Session->setFlash(__('Encadre deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Encadre was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Encadre->recursive = 0;
		$this->set('encadres', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Encadre->exists($id)) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		$options = array('conditions' => array('Encadre.' . $this->Encadre->primaryKey => $id));
		$this->set('encadre', $this->Encadre->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Encadre->create();
			if ($this->Encadre->save($this->request->data)) {
				$this->Session->setFlash(__('The encadre has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The encadre could not be saved. Please, try again.'));
			}
		}
		$matieres = $this->Encadre->Matiere->find('list');
		$enseignants = $this->Encadre->Enseignant->find('list');
		$classees = $this->Encadre->Classee->find('list');
		$this->set(compact('matieres', 'enseignants', 'classees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Encadre->exists($id)) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Encadre->save($this->request->data)) {
				$this->Session->setFlash(__('The encadre has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The encadre could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Encadre.' . $this->Encadre->primaryKey => $id));
			$this->request->data = $this->Encadre->find('first', $options);
		}
		$matieres = $this->Encadre->Matiere->find('list');
		$enseignants = $this->Encadre->Enseignant->find('list');
		$classees = $this->Encadre->Classee->find('list');
		$this->set(compact('matieres', 'enseignants', 'classees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Encadre->id = $id;
		if (!$this->Encadre->exists()) {
			throw new NotFoundException(__('Invalid encadre'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Encadre->delete()) {
			$this->Session->setFlash(__('Encadre deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Encadre was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
