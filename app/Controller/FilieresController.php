<?php
App::uses('AppController', 'Controller');
/**
 * Filieres Controller
 *
 * @property Filiere $Filiere
 */
class FilieresController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Filiere->recursive = 0;
		$this->set('filieres', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Filiere->exists($id)) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		$options = array('conditions' => array('Filiere.' . $this->Filiere->primaryKey => $id));
		$this->set('filiere', $this->Filiere->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Filiere->create();
			if ($this->Filiere->save($this->request->data)) {
				$this->Session->setFlash(__('The filiere has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The filiere could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->Filiere->exists($id)) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Filiere->save($this->request->data)) {
				$this->Session->setFlash(__('The filiere has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The filiere could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Filiere.' . $this->Filiere->primaryKey => $id));
			$this->request->data = $this->Filiere->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Filiere->id = $id;
		if (!$this->Filiere->exists()) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Filiere->delete()) {
			$this->Session->setFlash(__('Filiere deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Filiere was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

	$this->layout="admin";

		$this->Filiere->recursive = 0;
		$this->set('filieres', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {

	$this->layout="admin";

		if (!$this->Filiere->exists($id)) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		$options = array('conditions' => array('Filiere.' . $this->Filiere->primaryKey => $id));
		$this->set('filiere', $this->Filiere->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */

	public function admin_add() {
	$this->layout="admin";

		if ($this->request->is('post')) {
			$this->Filiere->create();
			if ($this->Filiere->save($this->request->data)) {
				$this->Session->setFlash(__('The filiere has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The filiere could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_edit($id = null) {
	$this->layout="admin";

		if (!$this->Filiere->exists($id)) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Filiere->save($this->request->data)) {
				$this->Session->setFlash(__('The filiere has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The filiere could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Filiere.' . $this->Filiere->primaryKey => $id));
			$this->request->data = $this->Filiere->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */

	public function admin_delete($id = null) {
	$this->layout="admin";

		$this->Filiere->id = $id;
		if (!$this->Filiere->exists()) {
			throw new NotFoundException(__('Invalid filiere'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Filiere->delete()) {
			$this->Session->setFlash(__('Filiere deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Filiere was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
