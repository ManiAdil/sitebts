<?php
App::uses('AppController', 'Controller');
/**
 * Absences Controller
 *
 * @property Absence $Absence
 */
class AbsencesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Absence->recursive = 0;
		$this->set('absences', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Absence->exists($id)) {
			throw new NotFoundException(__('Invalid absence'));
		}
		$options = array('conditions' => array('Absence.' . $this->Absence->primaryKey => $id));
		$this->set('absence', $this->Absence->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Absence->create();
			if ($this->Absence->save($this->request->data)) {
				$this->Session->setFlash(__('The absence has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The absence could not be saved. Please, try again.'));
			}
		}
		$enseignants = $this->Absence->Enseignant->find('list');
		$matieres = $this->Absence->Matiere->find('list');
		$etudiants = $this->Absence->Etudiant->find('list');
		$this->set(compact('enseignants', 'matieres', 'etudiants'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Absence->exists($id)) {
			throw new NotFoundException(__('Invalid absence'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Absence->save($this->request->data)) {
				$this->Session->setFlash(__('The absence has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The absence could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Absence.' . $this->Absence->primaryKey => $id));
			$this->request->data = $this->Absence->find('first', $options);
		}
		$enseignants = $this->Absence->Enseignant->find('list');
		$matieres = $this->Absence->Matiere->find('list');
		$etudiants = $this->Absence->Etudiant->find('list');
		$this->set(compact('enseignants', 'matieres', 'etudiants'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Absence->id = $id;
		if (!$this->Absence->exists()) {
			throw new NotFoundException(__('Invalid absence'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Absence->delete()) {
			$this->Session->setFlash(__('Absence deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Absence was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Absence->recursive = 0;
		$this->set('absences', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Absence->exists($id)) {
			throw new NotFoundException(__('Invalid absence'));
		}
		$options = array('conditions' => array('Absence.' . $this->Absence->primaryKey => $id));
		$this->set('absence', $this->Absence->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
		if ($this->request->is('post')) {
			$this->Absence->create();
			if ($this->Absence->save($this->request->data)) {
				$this->Session->setFlash(__('The absence has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The absence could not be saved. Please, try again.'));
			}
		}
		$enseignants = $this->Absence->Enseignant->find('list');
		$matieres = $this->Absence->Matiere->find('list');
		$etudiants = $this->Absence->Etudiant->find('list');
		$this->set(compact('enseignants', 'matieres', 'etudiants'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Absence->exists($id)) {
			throw new NotFoundException(__('Invalid absence'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Absence->save($this->request->data)) {
				$this->Session->setFlash(__('The absence has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The absence could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Absence.' . $this->Absence->primaryKey => $id));
			$this->request->data = $this->Absence->find('first', $options);
		}
		$enseignants = $this->Absence->Enseignant->find('list');
		$matieres = $this->Absence->Matiere->find('list');
		$etudiants = $this->Absence->Etudiant->find('list');
		$this->set(compact('enseignants', 'matieres', 'etudiants'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Absence->id = $id;
		if (!$this->Absence->exists()) {
			throw new NotFoundException(__('Invalid absence'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Absence->delete()) {
			$this->Session->setFlash(__('Absence deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Absence was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
