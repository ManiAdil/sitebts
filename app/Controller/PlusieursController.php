<?php
App::uses('AppController', 'Controller');
/**
 * Plusieurs Controller
 *
 * @property Plusieur $Plusieur
 */
class PlusieursController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Plusieur->recursive = 0;
		$this->set('plusieurs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Plusieur->exists($id)) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		$options = array('conditions' => array('Plusieur.' . $this->Plusieur->primaryKey => $id));
		$this->set('plusieur', $this->Plusieur->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Plusieur->create();
			if ($this->Plusieur->save($this->request->data)) {
				$this->Session->setFlash(__('The plusieur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plusieur could not be saved. Please, try again.'));
			}
		}
		$modules = $this->Plusieur->Module->find('list');
		$unites = $this->Plusieur->Unite->find('list');
		$this->set(compact('modules', 'unites'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Plusieur->exists($id)) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Plusieur->save($this->request->data)) {
				$this->Session->setFlash(__('The plusieur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plusieur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plusieur.' . $this->Plusieur->primaryKey => $id));
			$this->request->data = $this->Plusieur->find('first', $options);
		}
		$modules = $this->Plusieur->Module->find('list');
		$unites = $this->Plusieur->Unite->find('list');
		$this->set(compact('modules', 'unites'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Plusieur->id = $id;
		if (!$this->Plusieur->exists()) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Plusieur->delete()) {
			$this->Session->setFlash(__('Plusieur deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Plusieur was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Plusieur->recursive = 0;
		$this->set('plusieurs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Plusieur->exists($id)) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		$options = array('conditions' => array('Plusieur.' . $this->Plusieur->primaryKey => $id));
		$this->set('plusieur', $this->Plusieur->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Plusieur->create();
			if ($this->Plusieur->save($this->request->data)) {
				$this->Session->setFlash(__('The plusieur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plusieur could not be saved. Please, try again.'));
			}
		}
		$modules = $this->Plusieur->Module->find('list');
		$unites = $this->Plusieur->Unite->find('list');
		$this->set(compact('modules', 'unites'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Plusieur->exists($id)) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Plusieur->save($this->request->data)) {
				$this->Session->setFlash(__('The plusieur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plusieur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plusieur.' . $this->Plusieur->primaryKey => $id));
			$this->request->data = $this->Plusieur->find('first', $options);
		}
		$modules = $this->Plusieur->Module->find('list');
		$unites = $this->Plusieur->Unite->find('list');
		$this->set(compact('modules', 'unites'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Plusieur->id = $id;
		if (!$this->Plusieur->exists()) {
			throw new NotFoundException(__('Invalid plusieur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Plusieur->delete()) {
			$this->Session->setFlash(__('Plusieur deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Plusieur was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
