<?php
App::uses('AppController', 'Controller');
/**
 * Contients Controller
 *
 * @property Contient $Contient
 */
class ContientsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contient->recursive = 0;
		$this->set('contients', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Contient->exists($id)) {
			throw new NotFoundException(__('Invalid contient'));
		}
		$options = array('conditions' => array('Contient.' . $this->Contient->primaryKey => $id));
		$this->set('contient', $this->Contient->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contient->create();
			if ($this->Contient->save($this->request->data)) {
				$this->Session->setFlash(__('The contient has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contient could not be saved. Please, try again.'));
			}
		}
		$modules = $this->Contient->Module->find('list');
		$matieres = $this->Contient->Matiere->find('list');
		$this->set(compact('modules', 'matieres'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Contient->exists($id)) {
			throw new NotFoundException(__('Invalid contient'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contient->save($this->request->data)) {
				$this->Session->setFlash(__('The contient has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contient could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contient.' . $this->Contient->primaryKey => $id));
			$this->request->data = $this->Contient->find('first', $options);
		}
		$modules = $this->Contient->Module->find('list');
		$matieres = $this->Contient->Matiere->find('list');
		$this->set(compact('modules', 'matieres'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Contient->id = $id;
		if (!$this->Contient->exists()) {
			throw new NotFoundException(__('Invalid contient'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contient->delete()) {
			$this->Session->setFlash(__('Contient deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contient was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Contient->recursive = 0;
		$this->set('contients', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Contient->exists($id)) {
			throw new NotFoundException(__('Invalid contient'));
		}
		$options = array('conditions' => array('Contient.' . $this->Contient->primaryKey => $id));
		$this->set('contient', $this->Contient->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Contient->create();
			if ($this->Contient->save($this->request->data)) {
				$this->Session->setFlash(__('The contient has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contient could not be saved. Please, try again.'));
			}
		}
		$modules = $this->Contient->Module->find('list');
		$matieres = $this->Contient->Matiere->find('list');
		$this->set(compact('modules', 'matieres'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Contient->exists($id)) {
			throw new NotFoundException(__('Invalid contient'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contient->save($this->request->data)) {
				$this->Session->setFlash(__('The contient has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contient could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contient.' . $this->Contient->primaryKey => $id));
			$this->request->data = $this->Contient->find('first', $options);
		}
		$modules = $this->Contient->Module->find('list');
		$matieres = $this->Contient->Matiere->find('list');
		$this->set(compact('modules', 'matieres'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Contient->id = $id;
		if (!$this->Contient->exists()) {
			throw new NotFoundException(__('Invalid contient'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contient->delete()) {
			$this->Session->setFlash(__('Contient deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contient was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
