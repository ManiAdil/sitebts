<?php
App::uses('AppController', 'Controller');
/**
 * Classees Controller
 *
 * @property Classee $Classee
 */
class ClasseesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Classee->recursive = 0;
		$this->set('classees', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Classee->exists($id)) {
			throw new NotFoundException(__('Invalid classee'));
		}
		$options = array('conditions' => array('Classee.' . $this->Classee->primaryKey => $id));
		$this->set('classee', $this->Classee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Classee->create();
			if ($this->Classee->save($this->request->data)) {
				$this->Session->setFlash(__('The classee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classee could not be saved. Please, try again.'));
			}
		}
		$filieres = $this->Classee->Filiere->find('list');
		$niveaus = $this->Classee->Niveau->find('list');
		$this->set(compact('filieres', 'niveaus'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Classee->exists($id)) {
			throw new NotFoundException(__('Invalid classee'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Classee->save($this->request->data)) {
				$this->Session->setFlash(__('The classee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Classee.' . $this->Classee->primaryKey => $id));
			$this->request->data = $this->Classee->find('first', $options);
		}
		$filieres = $this->Classee->Filiere->find('list');
		$niveaus = $this->Classee->Niveau->find('list');
		$this->set(compact('filieres', 'niveaus'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Classee->id = $id;
		if (!$this->Classee->exists()) {
			throw new NotFoundException(__('Invalid classee'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Classee->delete()) {
			$this->Session->setFlash(__('Classee deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Classee was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() { $this->layout="admin" ;
		$this->Classee->recursive = 0;
		$this->set('classees', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Classee->exists($id)) {
			throw new NotFoundException(__('Invalid classee'));
		}
		$options = array('conditions' => array('Classee.' . $this->Classee->primaryKey => $id));
		$this->set('classee', $this->Classee->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Classee->create();
			if ($this->Classee->save($this->request->data)) {
				$this->Session->setFlash(__('The classee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classee could not be saved. Please, try again.'));
			}
		}
		$filieres = $this->Classee->Filiere->find('list');
		$niveaus = $this->Classee->Niveau->find('list');
		$this->set(compact('filieres', 'niveaus'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Classee->exists($id)) {
			throw new NotFoundException(__('Invalid classee'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Classee->save($this->request->data)) {
				$this->Session->setFlash(__('The classee has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The classee could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Classee.' . $this->Classee->primaryKey => $id));
			$this->request->data = $this->Classee->find('first', $options);
		}
		$filieres = $this->Classee->Filiere->find('list');
		$niveaus = $this->Classee->Niveau->find('list');
		$this->set(compact('filieres', 'niveaus'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Classee->id = $id;
		if (!$this->Classee->exists()) {
			throw new NotFoundException(__('Invalid classee'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Classee->delete()) {
			$this->Session->setFlash(__('Classee deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Classee was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
