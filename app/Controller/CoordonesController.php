<?php
App::uses('AppController', 'Controller');
/**
 * Coordones Controller
 *
 * @property Coordone $Coordone
 */
class CoordonesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Coordone->recursive = 0;
		$this->set('coordones', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Coordone->exists($id)) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		$options = array('conditions' => array('Coordone.' . $this->Coordone->primaryKey => $id));
		$this->set('coordone', $this->Coordone->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Coordone->create();
			if ($this->Coordone->save($this->request->data)) {
				$this->Session->setFlash(__('The coordone has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coordone could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Coordone->exists($id)) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Coordone->save($this->request->data)) {
				$this->Session->setFlash(__('The coordone has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coordone could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Coordone.' . $this->Coordone->primaryKey => $id));
			$this->request->data = $this->Coordone->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Coordone->id = $id;
		if (!$this->Coordone->exists()) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Coordone->delete()) {
			$this->Session->setFlash(__('Coordone deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Coordone was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() { $this->layout="admin" ;
		$this->Coordone->recursive = 0;
		$this->set('coordones', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) { $this->layout="admin" ;
		if (!$this->Coordone->exists($id)) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		$options = array('conditions' => array('Coordone.' . $this->Coordone->primaryKey => $id));
		$this->set('coordone', $this->Coordone->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() { $this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Coordone->create();
			if ($this->Coordone->save($this->request->data)) {
				$this->Session->setFlash(__('The coordone has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coordone could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) { $this->layout="admin" ;
		if (!$this->Coordone->exists($id)) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Coordone->save($this->request->data)) {
				$this->Session->setFlash(__('The coordone has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coordone could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Coordone.' . $this->Coordone->primaryKey => $id));
			$this->request->data = $this->Coordone->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) { $this->layout="admin" ;
		$this->Coordone->id = $id;
		if (!$this->Coordone->exists()) {
			throw new NotFoundException(__('Invalid coordone'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Coordone->delete()) {
			$this->Session->setFlash(__('Coordone deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Coordone was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
