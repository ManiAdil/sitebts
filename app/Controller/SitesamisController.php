<?php
App::uses('AppController', 'Controller');
/**
 * Sitesamis Controller
 *
 * @property Sitesami $Sitesami
 */
class SitesamisController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Sitesami->recursive = 0;
		$this->set('sitesamis', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Sitesami->exists($id)) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		$options = array('conditions' => array('Sitesami.' . $this->Sitesami->primaryKey => $id));
		$this->set('sitesami', $this->Sitesami->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Sitesami->create();
			if ($this->Sitesami->save($this->request->data)) {
				$this->flash(__('Sitesami saved.'), array('action' => 'index'));
			} else {
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Sitesami->exists($id)) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sitesami->save($this->request->data)) {
				$this->flash(__('The sitesami has been saved.'), array('action' => 'index'));
			} else {
			}
		} else {
			$options = array('conditions' => array('Sitesami.' . $this->Sitesami->primaryKey => $id));
			$this->request->data = $this->Sitesami->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Sitesami->id = $id;
		if (!$this->Sitesami->exists()) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Sitesami->delete()) {
			$this->flash(__('Sitesami deleted'), array('action' => 'index'));
		}
		$this->flash(__('Sitesami was not deleted'), array('action' => 'index'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	     $this->layout="admin";
		$this->Sitesami->recursive = 0;
		$this->set('sitesamis', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	     $this->layout="admin";
		if (!$this->Sitesami->exists($id)) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		$options = array('conditions' => array('Sitesami.' . $this->Sitesami->primaryKey => $id));
		$this->set('sitesami', $this->Sitesami->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	     $this->layout="admin";
		if ($this->request->is('post')) {
			$this->Sitesami->create();
			if ($this->Sitesami->save($this->request->data)) {
				$this->flash(__('Sitesami saved.'), array('action' => 'index'));
			} else {
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	     $this->layout="admin";
		if (!$this->Sitesami->exists($id)) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Sitesami->save($this->request->data)) {
				$this->flash(__('The sitesami has been saved.'), array('action' => 'index'));
			} else {
			}
		} else {
			$options = array('conditions' => array('Sitesami.' . $this->Sitesami->primaryKey => $id));
			$this->request->data = $this->Sitesami->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	  $this->layout="admin";
		$this->Mission->id = $id;
		if (!$this->Mission->exists()) {
			throw new NotFoundException(__('Invalid sitesami'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mission->delete()) {
			$this->Session->setFlash(__('Sitesami deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Sitesami was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
