<?php
App::uses('AppController', 'Controller');
/**
 * Etudiants Controller
 *
 * @property Etudiant $Etudiant
 */
class EtudiantsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
	
		$this->Etudiant->recursive = 0;
		$this->set('etudiants', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	
		if (!$this->Etudiant->exists($id)) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		$options = array('conditions' => array('Etudiant.' . $this->Etudiant->primaryKey => $id));
		$this->set('etudiant', $this->Etudiant->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	
		if ($this->request->is('post')) {
			$this->Etudiant->create();
			if ($this->Etudiant->save($this->request->data)) {
				$this->Session->setFlash(__('The etudiant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudiant could not be saved. Please, try again.'));
			}
		}
		$filieres = $this->Etudiant->Filiere->find('list');
		$this->set(compact('filieres'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	
		if (!$this->Etudiant->exists($id)) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Etudiant->save($this->request->data)) {
				$this->Session->setFlash(__('The etudiant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudiant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Etudiant.' . $this->Etudiant->primaryKey => $id));
			$this->request->data = $this->Etudiant->find('first', $options);
		}
		$filieres = $this->Etudiant->Filiere->find('list');
		$this->set(compact('filieres'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
	$this->layout="admin";
		$this->Etudiant->id = $id;
		if (!$this->Etudiant->exists()) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Etudiant->delete()) {
			$this->Session->setFlash(__('Etudiant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Etudiant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	$this->layout="admin";
		$this->Etudiant->recursive = 0;
		$this->set('etudiants', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	$this->layout="admin";
		if (!$this->Etudiant->exists($id)) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		$options = array('conditions' => array('Etudiant.' . $this->Etudiant->primaryKey => $id));
		$this->set('etudiant', $this->Etudiant->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	$this->layout="admin";
		if ($this->request->is('post')) {
		  $this->Upload->upload("Etudiant","imagefile","image");
			$this->Etudiant->create();
			if ($this->Etudiant->save($this->request->data)) {
				$this->Session->setFlash(__('The etudiant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudiant could not be saved. Please, try again.'));
			}
		}
		$filieres = $this->Etudiant->Filiere->find('list');
		$this->set(compact('filieres'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	$this->layout="admin";
		if (!$this->Etudiant->exists($id)) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
		  $this->Upload->upload("Etudiant","imagefile","image");
			if ($this->Etudiant->save($this->request->data)) {
				$this->Session->setFlash(__('The etudiant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The etudiant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Etudiant.' . $this->Etudiant->primaryKey => $id));
			$this->request->data = $this->Etudiant->find('first', $options);
		}
		$filieres = $this->Etudiant->Filiere->find('list');
		$this->set(compact('filieres'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	$this->layout="admin";
		$this->Etudiant->id = $id;
		if (!$this->Etudiant->exists()) {
			throw new NotFoundException(__('Invalid etudiant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Etudiant->delete()) {
			$this->Session->setFlash(__('Etudiant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Etudiant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
