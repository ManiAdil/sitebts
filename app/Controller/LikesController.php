<?php
App::uses('AppController', 'Controller');
/**
 * Likes Controller
 *
 * @property Like $Like
 */
class LikesController extends AppController {
    public function admin_delete_like($id)
    {
        $this->uses[] = "Reponse";
  		$like = $this->Like->find("first",array("conditions"=>array("Like.reponse_id"=>$id,"Like.user_id"=>$this->Session->read('id'))));
        $this->Like->delete($like["Like"]["id"]);
         $reponse = $this->Reponse->find("first",array("conditions"=>array("Reponse.id"=>$id)));
			
$this->redirect(array('action' => 'afficherquestion/'.$reponse['Reponse']['question_id'],"controller"=>"questions"));
}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Like->recursive = 0;
		$this->set('likes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Like->exists($id)) {
			throw new NotFoundException(__('Invalid like'));
		}
		$options = array('conditions' => array('Like.' . $this->Like->primaryKey => $id));
		$this->set('like', $this->Like->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Like->create();
			if ($this->Like->save($this->request->data)) {
				$this->Session->setFlash(__('The like has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The like could not be saved. Please, try again.'));
			}
		}
		$reponses = $this->Like->Reponse->find('list');
		$users = $this->Like->User->find('list');
		$this->set(compact('reponses', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Like->exists($id)) {
			throw new NotFoundException(__('Invalid like'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Like->save($this->request->data)) {
				$this->Session->setFlash(__('The like has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The like could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Like.' . $this->Like->primaryKey => $id));
			$this->request->data = $this->Like->find('first', $options);
		}
		$reponses = $this->Like->Reponse->find('list');
		$users = $this->Like->User->find('list');
		$this->set(compact('reponses', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Like->id = $id;
		if (!$this->Like->exists()) {
			throw new NotFoundException(__('Invalid like'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Like->delete()) {
			$this->Session->setFlash(__('Like deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Like was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Like->recursive = 0;
		$this->set('likes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Like->exists($id)) {
			throw new NotFoundException(__('Invalid like'));
		}
		$options = array('conditions' => array('Like.' . $this->Like->primaryKey => $id));
		$this->set('like', $this->Like->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	   $this->uses[] = "Reponse";
		if ($this->request->is('post')) {
			$this->Like->create();
			if ($this->Like->save($this->request->data)) {
			 $reponse = $this->Reponse->find("first",array("conditions"=>array("Reponse.id"=>$this->request->data['Like']['reponse_id'])));
				$this->Session->setFlash(__('The like has been saved'));
				$this->redirect(array('action' => 'afficherquestion/'.$reponse['Reponse']['question_id'],"controller"=>"questions"));
			} else {
				$this->Session->setFlash(__('The like could not be saved. Please, try again.'));
			}
		}
		$reponses = $this->Like->Reponse->find('list');
		$users = $this->Like->User->find('list');
		$this->set(compact('reponses', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Like->exists($id)) {
			throw new NotFoundException(__('Invalid like'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Like->save($this->request->data)) {
				$this->Session->setFlash(__('The like has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The like could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Like.' . $this->Like->primaryKey => $id));
			$this->request->data = $this->Like->find('first', $options);
		}
		$reponses = $this->Like->Reponse->find('list');
		$users = $this->Like->User->find('list');
		$this->set(compact('reponses', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Like->id = $id;
		if (!$this->Like->exists()) {
			throw new NotFoundException(__('Invalid like'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Like->delete()) {
			$this->Session->setFlash(__('Like deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Like was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
