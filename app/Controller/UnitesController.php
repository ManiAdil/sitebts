<?php
App::uses('AppController', 'Controller');
/**
 * Unites Controller
 *
 * @property Unite $Unite
 */
class UnitesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Unite->recursive = 0;
		$this->set('unites', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Unite->exists($id)) {
			throw new NotFoundException(__('Invalid unite'));
		}
		$options = array('conditions' => array('Unite.' . $this->Unite->primaryKey => $id));
		$this->set('unite', $this->Unite->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Unite->create();
			if ($this->Unite->save($this->request->data)) {
				$this->Session->setFlash(__('The unite has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unite could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Unite->exists($id)) {
			throw new NotFoundException(__('Invalid unite'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Unite->save($this->request->data)) {
				$this->Session->setFlash(__('The unite has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unite could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Unite.' . $this->Unite->primaryKey => $id));
			$this->request->data = $this->Unite->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Unite->id = $id;
		if (!$this->Unite->exists()) {
			throw new NotFoundException(__('Invalid unite'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Unite->delete()) {
			$this->Session->setFlash(__('Unite deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Unite was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Unite->recursive = 0;
		$this->set('unites', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Unite->exists($id)) {
			throw new NotFoundException(__('Invalid unite'));
		}
		$options = array('conditions' => array('Unite.' . $this->Unite->primaryKey => $id));
		$this->set('unite', $this->Unite->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Unite->create();
			if ($this->Unite->save($this->request->data)) {
				$this->Session->setFlash(__('The unite has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unite could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Unite->exists($id)) {
			throw new NotFoundException(__('Invalid unite'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Unite->save($this->request->data)) {
				$this->Session->setFlash(__('The unite has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The unite could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Unite.' . $this->Unite->primaryKey => $id));
			$this->request->data = $this->Unite->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Unite->id = $id;
		if (!$this->Unite->exists()) {
			throw new NotFoundException(__('Invalid unite'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Unite->delete()) {
			$this->Session->setFlash(__('Unite deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Unite was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
