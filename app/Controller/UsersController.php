
<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
public function login()
{
$this->layout = "admin";
    $this->set("title_for_layout");
}
public function logout()
{
    $this->layout = "admin";
    $this->Session->destroy();
    $this->redirect(array("controller"=>"users","action"=>"login"));
}
public function authentifier()
{
    $this->uses[]="Etudiant";
    $user = $this->User->find("all",array("conditions"=>array("username"=>$this->request->data["User"]["username"],"password"=>$this->request->data["User"]["password"])));

    if(!empty($user)){
    //$etudiant =  $this->Etudiant->find("first",array("conditions"=>array("username"=>$this->request->data["User"]["username"],"password"=>$this->request->data["User"]["password"])));

    if ($user[0]["User"]["group_id"]==1 or $user[0]["User"]["group_id"]==2)
    {
    
    $nom = $user[0]["Enseignant"][0]["nom"];
    $prenom = $user[0]["Enseignant"][0]["prenom"];
    $image = $user[0]["Enseignant"][0]["image"];
    $email = $user[0]["Enseignant"][0]["email"];
    $id = $user[0]["Enseignant"][0]["id"];
    
    }else
    {
    $nom = $user[0]["Etudiant"][0]["nom"];
    $prenom = $user[0]["Etudiant"][0]["prenom"];
    $image = $user[0]["Etudiant"][0]["image"];
    $email = $user[0]["Etudiant"][0]["email"];
    $id = $user[0]["Etudiant"][0]["id"];
    }
    $complet = $nom." ".$prenom;
    $this->Session->write('id', $id);
    $this->Session->write('role', $user[0]["User"]["group_id"]);
    $this->Session->write('complet', $complet);
    $this->Session->write('image', $image);
    $this->Session->write('email', $email);
    $this->redirect("/admin");
    //echo $vert = $this->Session->read('id');
    }
   
}


}

