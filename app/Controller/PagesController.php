<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
	
        $this->layout= "intro";
	
	}
    public function coordonee()
    {
               $this->set('title_for_layout','Nos coordonées - BTS Maghreb Arabi');
        
    }
        public function missions()
    {
               $this->uses[] = "Mission";
               $this->set("missions",$this->Mission->find("all"));
        
    }
         public function informations()
    {
               $this->uses[] = "Information";
               $this->set("informations",$this->Information->find("all"));
        
    }
     public function annonces()
    {
               $this->uses[]="Annonce";
               $this->set("annonces",$this->Annonce->find("all"));
        
    }
    public function etineraire()
    {
               $this->set('title_for_layout','Etinéraire - BTS Maghreb Arabi');
        
    }
    	public function contact() {
        $this->set('title_for_layout','Nous Envoyer Un Message - BTS Maghreb Arabi');
	
	}

	public function presentation_bts() {
	$this->set('title_for_layout','Présentation BTS - BTS Maghreb Arabi');
	}
	public function presentationdiplom(){
	
	$this->set('title_for_layout','Présentation des diplômes - BTS Maghreb Arabi');
	
	}
    	
		public function localisation(){
	
	$this->set('title_for_layout','Notre Localisation - BTS Maghreb Arabi');
	
	}
	public function conditionacces() {
	$this->set('title_for_layout','Nos conditionacces');
	}

	
	public function Reglement(){
	
	$this->set('title_for_layout','Reglement - BTS Maghreb Arabi');
	
	}
   
    public function admin(){
       $this->uses = array("Annonce","News");
        $this->layout= "admin";
        $this->set('news',$this->News->find("all"));
        $this->set('annonces',$this->Annonce->find("all"));
    }
     public function nos_coordones(){
       
         $this->uses[]="Coordone";
               $this->set("coordones",$this->Coordone->find("all"));
    }
	
    
     public function  news(){
       
        $this->uses[]= "News";
        $this->set("news", $this->News->find("all"));
    }
    
    
     public function  information_directeurs(){
       
        $this->uses[]= "infodirecteurs";
        $this->set("infodirecteurs", $this->Infodirecteur->find("all"));
    }

}
