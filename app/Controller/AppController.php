<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
private $widget= array();
public $components=array("Upload","Session");
    public function beforeFilter()
    {
        if($this->request->prefix=="admin")
        {
            if($this->Session->check('id')== true)
            {
                
            }else
            {
                $this->redirect(array("controller"=>"Users","admin"=>false,"action"=>"login"));
            }
        }
        $this->searchsitesamis();
        $this->searchMotDirecteur();
        $this->searchActualites();
        $this->set("widget",$this->widget);
    }
    private function searchsitesamis(){
      $this->uses[]= "Sitesami";
      $this->Sitesami->recursive = 0;
      $this->widget["Sitesamis"]= $this->Sitesami->find('all');
    }

    private function searchMotDirecteur(){
      $this->uses[]= "Infodirecteur";
      $this->Infodirecteur->recursive = 0;
      $this->widget["Infodirecteur"]= $this->Infodirecteur->find('first');
    }
    private function searchActualites(){
      $this->uses[]= "News";
      $this->News->recursive = 0;
      $this->widget["News"] = $this->News->find('all');
    }
}
