<?php
App::uses('AppController', 'Controller');
/**
 * Annonces Controller
 *
 * @property Annonce $Annonce
 */
class AnnoncesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Annonce->recursive = 0;
		$this->set('annonces', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Annonce->exists($id)) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		$options = array('conditions' => array('Annonce.' . $this->Annonce->primaryKey => $id));
		$this->set('annonce', $this->Annonce->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Annonce->create();
			if ($this->Annonce->save($this->request->data)) {
				$this->Session->setFlash(__('The annonce has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annonce could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Annonce->exists($id)) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Annonce->save($this->request->data)) {
				$this->Session->setFlash(__('The annonce has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annonce could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Annonce.' . $this->Annonce->primaryKey => $id));
			$this->request->data = $this->Annonce->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Annonce->id = $id;
		if (!$this->Annonce->exists()) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Annonce->delete()) {
			$this->Session->setFlash(__('Annonce deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Annonce was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	   $this->layout="admin";
	   
		$this->Annonce->recursive = 0;
		$this->set('annonces', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	     $this->layout="admin";
		if (!$this->Annonce->exists($id)) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		$options = array('conditions' => array('Annonce.' . $this->Annonce->primaryKey => $id));
		$this->set('annonce', $this->Annonce->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	     $this->layout="admin";
		if ($this->request->is('post')) {
			$this->Annonce->create();
			if ($this->Annonce->save($this->request->data)) {
				
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annonce could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	     $this->layout="admin";
		if (!$this->Annonce->exists($id)) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Annonce->save($this->request->data)) {
			
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annonce could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Annonce.' . $this->Annonce->primaryKey => $id));
			$this->request->data = $this->Annonce->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	     $this->layout="admin";
		$this->Annonce->id = $id;
		if (!$this->Annonce->exists()) {
			throw new NotFoundException(__('Invalid annonce'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Annonce->delete()) {
		
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Annonce was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
