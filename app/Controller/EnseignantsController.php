<?php
App::uses('AppController', 'Controller');
/**
 * Enseignants Controller
 *
 * @property Enseignant $Enseignant
 */
class EnseignantsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Enseignant->recursive = 0;
		$this->set('enseignants', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Enseignant->exists($id)) {
			throw new NotFoundException(__('Invalid enseignant'));
		}
		$options = array('conditions' => array('Enseignant.' . $this->Enseignant->primaryKey => $id));
		$this->set('enseignant', $this->Enseignant->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Enseignant->create();
			if ($this->Enseignant->save($this->request->data)) {
				$this->Session->setFlash(__('The enseignant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseignant could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Enseignant->exists($id)) {
			throw new NotFoundException(__('Invalid enseignant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Enseignant->save($this->request->data)) {
				$this->Session->setFlash(__('The enseignant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseignant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Enseignant.' . $this->Enseignant->primaryKey => $id));
			$this->request->data = $this->Enseignant->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Enseignant->id = $id;
		if (!$this->Enseignant->exists()) {
			throw new NotFoundException(__('Invalid enseignant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Enseignant->delete()) {
			$this->Session->setFlash(__('Enseignant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Enseignant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {Cache::clear();
	   $this->layout="admin" ;
		$this->Enseignant->recursive = 0;
		$this->set('enseignants', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	   $this->layout="admin" ;
		if (!$this->Enseignant->exists($id)) {
			throw new NotFoundException(__('Invalid enseignant'));
		}
		$options = array('conditions' => array('Enseignant.' . $this->Enseignant->primaryKey => $id));
		$this->set('enseignant', $this->Enseignant->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
    
		if ($this->request->is('post')) {$this->Upload->upload("Enseignant","imagefile","image");
			$this->Enseignant->create();
			if ($this->Enseignant->save($this->request->data)) {
				$this->Session->setFlash(__('The enseignant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseignant could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
    
		if (!$this->Enseignant->exists($id)) {
		
			throw new NotFoundException(__('Invalid enseignant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {  
		  $this->Upload->upload("Enseignant","imagefile","image");
			if ($this->Enseignant->save($this->request->data)) {
				$this->Session->setFlash(__('The enseignant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseignant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Enseignant.' . $this->Enseignant->primaryKey => $id));
			$this->request->data = $this->Enseignant->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Enseignant->id = $id;
		if (!$this->Enseignant->exists()) {
			throw new NotFoundException(__('Invalid enseignant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Enseignant->delete()) {
			$this->Session->setFlash(__('Enseignant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Enseignant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
