<?php
App::uses('AppController', 'Controller');
/**
 * Niveaus Controller
 *
 * @property Niveau $Niveau
 */
class NiveausController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Niveau->recursive = 0;
		$this->set('niveaus', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Niveau->exists($id)) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		$options = array('conditions' => array('Niveau.' . $this->Niveau->primaryKey => $id));
		$this->set('niveau', $this->Niveau->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Niveau->create();
			if ($this->Niveau->save($this->request->data)) {
				$this->Session->setFlash(__('The niveau has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The niveau could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Niveau->exists($id)) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Niveau->save($this->request->data)) {
				$this->Session->setFlash(__('The niveau has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The niveau could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Niveau.' . $this->Niveau->primaryKey => $id));
			$this->request->data = $this->Niveau->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Niveau->id = $id;
		if (!$this->Niveau->exists()) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Niveau->delete()) {
			$this->Session->setFlash(__('Niveau deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Niveau was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Niveau->recursive = 0;
		$this->set('niveaus', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Niveau->exists($id)) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		$options = array('conditions' => array('Niveau.' . $this->Niveau->primaryKey => $id));
		$this->set('niveau', $this->Niveau->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() { $this->layout="admin";
		if ($this->request->is('post')) { 
			$this->Niveau->create();
			if ($this->Niveau->save($this->request->data)) {
				$this->Session->setFlash(__('The niveau has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The niveau could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Niveau->exists($id)) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Niveau->save($this->request->data)) {
				$this->Session->setFlash(__('The niveau has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The niveau could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Niveau.' . $this->Niveau->primaryKey => $id));
			$this->request->data = $this->Niveau->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Niveau->id = $id;
		if (!$this->Niveau->exists()) {
			throw new NotFoundException(__('Invalid niveau'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Niveau->delete()) {
			$this->Session->setFlash(__('Niveau deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Niveau was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
