<?php
App::uses('AppController', 'Controller');
/**
 * Reponses Controller
 *
 * @property Reponse $Reponse
 */
class ReponsesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Reponse->recursive = 0;
		$this->set('reponses', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reponse->exists($id)) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		$options = array('conditions' => array('Reponse.' . $this->Reponse->primaryKey => $id));
		$this->set('reponse', $this->Reponse->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Reponse->create();
			if ($this->Reponse->save($this->request->data)) {
				$this->Session->setFlash(__('The reponse has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reponse could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Reponse->exists($id)) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Reponse->save($this->request->data)) {
				$this->Session->setFlash(__('The reponse has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reponse could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Reponse.' . $this->Reponse->primaryKey => $id));
			$this->request->data = $this->Reponse->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Reponse->id = $id;
		if (!$this->Reponse->exists()) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Reponse->delete()) {
			$this->Session->setFlash(__('Reponse deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Reponse was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {  $this->layout="admin" ;
	
		$this->set('reponses', $this->Reponse->find("all"));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) { $this->layout="admin" ;
		if (!$this->Reponse->exists($id)) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		$options = array('conditions' => array('Reponse.' . $this->Reponse->primaryKey => $id));
		$this->set('reponse', $this->Reponse->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() { $this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Reponse->create();
			if ($this->Reponse->save($this->request->data)) {
			
				$this->Session->setFlash(__('The reponse has been saved'));
                $action = "afficherquestion/".$this->request->data["Reponse"]["question_id"];
				$this->redirect(array('action' => $action,'controller'=>'questions',));
			} else {
				$this->Session->setFlash(__('The reponse could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) { $this->layout="admin" ;
		if (!$this->Reponse->exists($id)) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Reponse->save($this->request->data)) {
				$this->Session->setFlash(__('The reponse has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reponse could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Reponse.' . $this->Reponse->primaryKey => $id));
			$this->request->data = $this->Reponse->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) { $this->layout="admin" ;
		$this->Reponse->id = $id;


		if (!$this->Reponse->exists()) {
			throw new NotFoundException(__('Invalid reponse'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Reponse->delete()) {
			$this->Session->setFlash(__('Reponse deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Reponse was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
