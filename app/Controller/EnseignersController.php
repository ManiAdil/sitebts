<?php
App::uses('AppController', 'Controller');
/**
 * Enseigners Controller
 *
 * @property Enseigner $Enseigner
 */
class EnseignersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Enseigner->recursive = 0;
		$this->set('enseigners', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Enseigner->exists($id)) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		$options = array('conditions' => array('Enseigner.' . $this->Enseigner->primaryKey => $id));
		$this->set('enseigner', $this->Enseigner->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Enseigner->create();
			if ($this->Enseigner->save($this->request->data)) {
				$this->Session->setFlash(__('The enseigner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseigner could not be saved. Please, try again.'));
			}
		}
		$classees = $this->Enseigner->Classee->find('list');
		$enseignants = $this->Enseigner->Enseignant->find('list');
		$this->set(compact('classees', 'enseignants'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Enseigner->exists($id)) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Enseigner->save($this->request->data)) {
				$this->Session->setFlash(__('The enseigner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseigner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Enseigner.' . $this->Enseigner->primaryKey => $id));
			$this->request->data = $this->Enseigner->find('first', $options);
		}
		$classees = $this->Enseigner->Classee->find('list');
		$enseignants = $this->Enseigner->Enseignant->find('list');
		$this->set(compact('classees', 'enseignants'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Enseigner->id = $id;
		if (!$this->Enseigner->exists()) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Enseigner->delete()) {
			$this->Session->setFlash(__('Enseigner deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Enseigner was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin" ;
		$this->Enseigner->recursive = 0;
		$this->set('enseigners', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin" ;
		if (!$this->Enseigner->exists($id)) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		$options = array('conditions' => array('Enseigner.' . $this->Enseigner->primaryKey => $id));
		$this->set('enseigner', $this->Enseigner->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin" ;
		if ($this->request->is('post')) {
			$this->Enseigner->create();
			if ($this->Enseigner->save($this->request->data)) {
				$this->Session->setFlash(__('The enseigner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseigner could not be saved. Please, try again.'));
			}
		}
		$classees = $this->Enseigner->Classee->find('list');
		$enseignants = $this->Enseigner->Enseignant->find('list');
		$this->set(compact('classees', 'enseignants'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin" ;
		if (!$this->Enseigner->exists($id)) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Enseigner->save($this->request->data)) {
				$this->Session->setFlash(__('The enseigner has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The enseigner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Enseigner.' . $this->Enseigner->primaryKey => $id));
			$this->request->data = $this->Enseigner->find('first', $options);
		}
		$classees = $this->Enseigner->Classee->find('list');
		$enseignants = $this->Enseigner->Enseignant->find('list');
		$this->set(compact('classees', 'enseignants'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin" ;
		$this->Enseigner->id = $id;
		if (!$this->Enseigner->exists()) {
			throw new NotFoundException(__('Invalid enseigner'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Enseigner->delete()) {
			$this->Session->setFlash(__('Enseigner deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Enseigner was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
