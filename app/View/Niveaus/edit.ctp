<div class="niveaus form">
<?php echo $this->Form->create('Niveau'); ?>
	<fieldset>
		<legend><?php echo __('Edit Niveau'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('niveau');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Niveau.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Niveau.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
