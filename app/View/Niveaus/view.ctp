<div class="niveaus view">
<h2><?php  echo __('Niveau'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($niveau['Niveau']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Niveau'); ?></dt>
		<dd>
			<?php echo h($niveau['Niveau']['niveau']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Niveau'), array('action' => 'edit', $niveau['Niveau']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Niveau'), array('action' => 'delete', $niveau['Niveau']['id']), null, __('Are you sure you want to delete # %s?', $niveau['Niveau']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Niveau'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Classees'); ?></h3>
	<?php if (!empty($niveau['Classee'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nom'); ?></th>
		<th><?php echo __('Filiere Id'); ?></th>
		<th><?php echo __('Niveau Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($niveau['Classee'] as $classee): ?>
		<tr>
			<td><?php echo $classee['id']; ?></td>
			<td><?php echo $classee['nom']; ?></td>
			<td><?php echo $classee['filiere_id']; ?></td>
			<td><?php echo $classee['niveau_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'classees', 'action' => 'view', $classee['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'classees', 'action' => 'edit', $classee['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'classees', 'action' => 'delete', $classee['id']), null, __('Are you sure you want to delete # %s?', $classee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
