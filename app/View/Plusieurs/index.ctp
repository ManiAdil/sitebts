<div class="plusieurs index">
	<h2><?php echo __('Plusieurs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('module_id'); ?></th>
			<th><?php echo $this->Paginator->sort('unite_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($plusieurs as $plusieur): ?>
	<tr>
		<td><?php echo h($plusieur['Plusieur']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($plusieur['Module']['id'], array('controller' => 'modules', 'action' => 'view', $plusieur['Module']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($plusieur['Unite']['id'], array('controller' => 'unites', 'action' => 'view', $plusieur['Unite']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $plusieur['Plusieur']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $plusieur['Plusieur']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $plusieur['Plusieur']['id']), null, __('Are you sure you want to delete # %s?', $plusieur['Plusieur']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Unites'), array('controller' => 'unites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unite'), array('controller' => 'unites', 'action' => 'add')); ?> </li>
	</ul>
</div>
