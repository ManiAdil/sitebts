
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste </a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Plusieurs","action"=>"add","admin"=>true));?>">Ajouter </a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste </h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>module</th>
<th>unite</th>
<th>Action</th>
</tr>
<?php foreach ($plusieurs as $plusieur): ?>
	<tr>
		<td><?php echo h($plusieur['Plusieur']['id']); ?>&nbsp;</td>
        <td><?php echo h($plusieur['Module']['nom']); ?>&nbsp;</td>
        <td><?php echo h($plusieur['Unite']['nom']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $plusieur['Plusieur']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $plusieur['Plusieur']['id']), null, __('Are you sure you want to delete # %s?', $plusieur['Plusieur']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>