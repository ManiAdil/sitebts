<div class="plusieurs view">
<h2><?php  echo __('Plusieur'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($plusieur['Plusieur']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Module'); ?></dt>
		<dd>
			<?php echo $this->Html->link($plusieur['Module']['id'], array('controller' => 'modules', 'action' => 'view', $plusieur['Module']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unite'); ?></dt>
		<dd>
			<?php echo $this->Html->link($plusieur['Unite']['id'], array('controller' => 'unites', 'action' => 'view', $plusieur['Unite']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Plusieur'), array('action' => 'edit', $plusieur['Plusieur']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Plusieur'), array('action' => 'delete', $plusieur['Plusieur']['id']), null, __('Are you sure you want to delete # %s?', $plusieur['Plusieur']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Unites'), array('controller' => 'unites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unite'), array('controller' => 'unites', 'action' => 'add')); ?> </li>
	</ul>
</div>
