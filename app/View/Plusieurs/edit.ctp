<div class="plusieurs form">
<?php echo $this->Form->create('Plusieur'); ?>
	<fieldset>
		<legend><?php echo __('Edit Plusieur'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('module_id');
		echo $this->Form->input('unite_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Plusieur.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Plusieur.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Unites'), array('controller' => 'unites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unite'), array('controller' => 'unites', 'action' => 'add')); ?> </li>
	</ul>
</div>
