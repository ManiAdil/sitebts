
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste des matieres</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Matieres","action"=>"add","admin"=>true));?>">Ajouter un nouvelle matiere</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste des matieres</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Nom</th>
<th>coefficient</th>
<th>Action</th>
</tr>
<?php foreach ($matieres as $matiere): ?>
	<tr>
		<td><?php echo h($matiere['Matiere']['id']); ?>&nbsp;</td>
        <td><?php echo h($matiere['Matiere']['nom']); ?>&nbsp;</td>
        <td><?php echo h($matiere['Matiere']['coeff']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $matiere['Matiere']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $matiere['Matiere']['id']), null, __('Are you sure you want to delete # %s?', $matiere['Matiere']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>