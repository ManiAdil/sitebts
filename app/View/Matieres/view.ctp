<div class="matieres view">
<h2><?php  echo __('Matiere'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($matiere['Matiere']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($matiere['Matiere']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Coeff'); ?></dt>
		<dd>
			<?php echo h($matiere['Matiere']['coeff']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Matiere'), array('action' => 'edit', $matiere['Matiere']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Matiere'), array('action' => 'delete', $matiere['Matiere']['id']), null, __('Are you sure you want to delete # %s?', $matiere['Matiere']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contients'), array('controller' => 'contients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudies'), array('controller' => 'etudies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudy'), array('controller' => 'etudies', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Contients'); ?></h3>
	<?php if (!empty($matiere['Contient'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Module Id'); ?></th>
		<th><?php echo __('Matiere Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($matiere['Contient'] as $contient): ?>
		<tr>
			<td><?php echo $contient['id']; ?></td>
			<td><?php echo $contient['module_id']; ?></td>
			<td><?php echo $contient['matiere_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'contients', 'action' => 'view', $contient['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'contients', 'action' => 'edit', $contient['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'contients', 'action' => 'delete', $contient['id']), null, __('Are you sure you want to delete # %s?', $contient['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Encadres'); ?></h3>
	<?php if (!empty($matiere['Encadre'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Matiere Id'); ?></th>
		<th><?php echo __('Enseignant Id'); ?></th>
		<th><?php echo __('Classe Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($matiere['Encadre'] as $encadre): ?>
		<tr>
			<td><?php echo $encadre['id']; ?></td>
			<td><?php echo $encadre['matiere_id']; ?></td>
			<td><?php echo $encadre['enseignant_id']; ?></td>
			<td><?php echo $encadre['classe_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'encadres', 'action' => 'view', $encadre['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'encadres', 'action' => 'edit', $encadre['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'encadres', 'action' => 'delete', $encadre['id']), null, __('Are you sure you want to delete # %s?', $encadre['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Etudies'); ?></h3>
	<?php if (!empty($matiere['Etudy'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Matiere Id'); ?></th>
		<th><?php echo __('Classe Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($matiere['Etudy'] as $etudy): ?>
		<tr>
			<td><?php echo $etudy['id']; ?></td>
			<td><?php echo $etudy['matiere_id']; ?></td>
			<td><?php echo $etudy['classe_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'etudies', 'action' => 'view', $etudy['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'etudies', 'action' => 'edit', $etudy['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'etudies', 'action' => 'delete', $etudy['id']), null, __('Are you sure you want to delete # %s?', $etudy['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Etudy'), array('controller' => 'etudies', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
