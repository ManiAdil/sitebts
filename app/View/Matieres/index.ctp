<div class="matieres index">
	<h2><?php echo __('Matieres'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th><?php echo $this->Paginator->sort('coeff'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($matieres as $matiere): ?>
	<tr>
		<td><?php echo h($matiere['Matiere']['id']); ?>&nbsp;</td>
		<td><?php echo h($matiere['Matiere']['nom']); ?>&nbsp;</td>
		<td><?php echo h($matiere['Matiere']['coeff']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $matiere['Matiere']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $matiere['Matiere']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $matiere['Matiere']['id']), null, __('Are you sure you want to delete # %s?', $matiere['Matiere']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Matiere'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Contients'), array('controller' => 'contients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudies'), array('controller' => 'etudies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudy'), array('controller' => 'etudies', 'action' => 'add')); ?> </li>
	</ul>
</div>
