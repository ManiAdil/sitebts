<div class="matieres form">
<?php echo $this->Form->create('Matiere'); ?>
	<fieldset>
		<legend><?php echo __('Add Matiere'); ?></legend>
	<?php
		echo $this->Form->input('nom');
		echo $this->Form->input('coeff');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Matieres'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Contients'), array('controller' => 'contients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudies'), array('controller' => 'etudies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudy'), array('controller' => 'etudies', 'action' => 'add')); ?> </li>
	</ul>
</div>
