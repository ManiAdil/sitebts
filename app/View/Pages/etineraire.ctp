  
<div id="container">
        <h1>Itinéraire entre votre place et lycée Technique Maghreb Arabi</h1>
        <div id="destinationForm">
            <form action="" method="get" name="direction" id="direction">
                <label>Point de départ :</label>
                <input type="text" name="origin" id="origin">
              
                <input type="hidden" name="destination" id="destination" value="Lycée Technique Maghreb Arabi Oujda">
                <input type="button" value="Calculer l'itinéraire" onclick="javascript:calculate()">
            </form>
        </div>
        <div id="panel"></div>
        <div id="map">
            <p>Veuillez patienter pendant le chargement de la carte...</p>
        </div>
    </div>
    
    <!-- Include Javascript -->
    
        <?php echo $this->Html->script("ci/jquery.min");?>
     <?php echo $this->Html->script("ci/jquery-ui-1.8.12.custom.min");?>
    
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=fr"></script>
    
    <?php echo $this->Html->script("ci/functions");?>
  

