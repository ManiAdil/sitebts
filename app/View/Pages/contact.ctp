 <article class="grid_14 suffix_1 omega">
                    <h2>Formulaire de contact:</h2>
                    <form id="contact-form" method="post">
                      <fieldset>
                        <label class="name">
                            <span>Full name:</span>
                            <input type="text">
                        </label>
                        <label class="email">
                          <span>Email:</span>
                          <input type="email">
                        </label>
                        <label class="message">
                          <span>Message:</span>	
                          <textarea></textarea>
                        </label>
                        <div class="btns">
                            <a class="link" onClick="document.getElementById('contact-form').reset()">Annuler</a>
                            <a class="link" onClick="document.getElementById('contact-form').submit()">Envoyer</a> 
                        </div>
                      </fieldset>
                    </form>
                </article>