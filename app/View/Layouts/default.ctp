<!DOCTYPE html>
<html lang="en">
<head>
  	<title> <?php echo $title_for_layout ?> </title>
  	<meta charset="utf-8">
    	<?php	echo $this->Html->css('style');?>
        <?php	echo $this->Html->css('grid');?>
     <style type="text/css">
   
    #container{width:99%;margin:auto;background:#FFFFFF;padding:20px 0px 20px 0px;float:left;}
    #container h1{margin:0px 0px 10px 20px;}
    #container #map{width:99%;height:500px;margin:auto;}
    #container #panel{width:99%;margin:auto;}
    #container #destinationForm{margin:0px 0px 20px 0px;background:#EEEEEE;padding:10px 20px;border:solid 1px #C0C0C0;}
    #container #destinationForm input[type=text]{border:solid 1px #C0C0C0;}
  </style>
  <?php	echo $this->Html->css('jquery-ui-1.8.12.custom');?>

      
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Your name">

    	<?php	echo $this->Html->script('jquery-1.7.1.min');?>
  	<?php	echo $this->Html->script('superfish');?>
  	<?php	echo $this->Html->script('jquery.cycle.all.min');?>
   	<?php	echo $this->Html->script('jquery.easing.1.3');?>
    
<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
<!--[if lt IE 9]>
	<?php	echo $this->Html->script('html5');?>
     <?php	echo $this->Html->css('ie');?>
<![endif]-->
</head>
<body>
<!-- Header -->
<header>
    <div class="inner">
        <?php echo $this->Html->image("bts.png",array("style"=>"left: -200px;position: relative;"));?>
	</div>
    <nav>
        <ul class="sf-menu">
            <li><a href="<?php echo $this->Html->url('/') ?>">acceuil</a></li>
            <li><a href="about.html">apropos de nous</a>
            <ul>
                    <li><a href="<?php echo $this->Html->url('/contactez-nous') ?>">Présentation lycée</a></li>
                    <li><a href="<?php echo $this->Html->url('/presentation_bts') ?>">Présentation BTS</a></li>
                    <li><a href="<?php echo $this->Html->url('/conditionacces') ?>">conditions </a></li>
                    <li><a href="<?php echo $this->Html->url('/Reglement') ?>"> réglement </a></li>
                </ul>
<li>

<a href="services.html">BTS OUJDA</a>
<ul>
<li><a href="#">Nos Missions</a></li>
<li><a href="#">Nos Services</a></li>
<li>

<a href="#">Nos Filières</a>
<ul>
<li><a href="#">Développement des Systèmes d'Information</a></li>
<li><a href="#">Systèmes et Réseaux Informatiques</a></li>
<li><a href="#">Electromecanique et Systeme Automatisé</a></li>
</ul>
</li>
<li><a href="/nos-infos">Nos Informations</a></li>
<li><a href="#">Nos Annoces</a></li>
</ul>

        
            <li><a href="contacts.html">contacts</a>
            <ul>
                    <li><a href="<?php echo $this->Html->url('/contactez-nous') ?>">Nous envoyer un message</a></li>
                    <li><a href="<?php echo $this->Html->url('/nos-coordonees') ?>">Nos Coordonées</a></li>
                    <li><a href="<?php echo $this->Html->url('/etineraire') ?>">Etinéraire</a></li>
                    <li><a href="<?php echo $this->Html->url('/notre-localisation') ?>">Notre Localisation</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $this->Html->url('/admin') ?>">Se connecter</a></li>
        </ul>
        <div class="clear"></div>
    </nav>
</header>
<!-- Content -->
<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - August 06, 2011!</div>
    <div class="container_24">
    	<div class="grid_24 content-border">
            <div class="top-content-box">
                <ul id="mp-pags-cont-extra">
                    <li><a href="#">Stratégies d'études</a></li>
                    <li><a href="#">Activités Récentes</a></li>
                    <li><a href="#">Perspectives et Future</a></li>
                </ul>
            </div>
            <div class="top-row">
            	<h1 class="aligncenter">Votre Future, Votre Carrière <br> Notre Résponsabilité.</h1>
                <div class="wrapper">
                	<div class="grid_5 prefix_1 alpha">
                    	<div class="wrapper">
                        	<span class="dropcap color-1">A.</span>
                            <div class="extra-wrap">
                            	<strong class="str-1 color-1">
                                Diplôme de technicien Supérieur en Développement
                                </strong>
                                <a href="#" class="link">Lire Plus</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid_5">
                    	<span class="dropcap color-2">B.</span>
                            <div class="extra-wrap">
                            <strong class="str-1 color-2">
                               Diplôme de technicien  supérieur   en   réseaux   informatiques
                            </strong>
                            <a href="#" class="link">Lire Plus</a>
                        </div>
                    </div>
                    <div class="grid_5">
                    	<span class="dropcap color-3">C.</span>
                        <div class="extra-wrap">
                            <strong class="str-1 color-3">
                               Diplôme de technicien  supérieur   en   gestion des entreprises
                            </strong>
                            <a href="#" class="link">read more</a>
                        </div>
                    </div>
                    <div class="grid_5 suffix_1 omega">
                    	<span class="dropcap color-4">D.</span>
                        <div class="extra-wrap">
                            <strong class="str-1 color-4">
                            Diplôme de technicien  supérieur   en   commerce et comptabilité                           </strong>
                            <a href="#" class="link">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper indent-top">
            	<article class="grid_15 suffix_1 prefix_1 alpha" style="width: 60%; float: left;">
             <?php echo $content_for_layout ?>
                </article>
                <?php echo $this->element('/widget'); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- Footer -->
<footer>
    <div class="copyright">
        &copy; 2012 <strong class="footer-logo">Pet clinic</strong>
        <div>Website Template designed by <a href="http://www.templatemonster.com/" rel="nofollow">TemplateMonster.com</a></div>
    </div>
    <ul class="social-list">
    	<li><a href="#"><?php echo $this->Html->image("social-link-1.jpg")?></a></li>
        <li><a href="#"><?php echo $this->Html->image("social-link-2.jpg")?></a></li>
        <li><a href="#"><?php echo $this->Html->image("social-link-3.jpg")?></a></li>
    </ul>
</footer>
</body>
</html>
