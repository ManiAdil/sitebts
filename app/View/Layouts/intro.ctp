<!DOCTYPE html>
<html lang="en">
<head>
<title>Acceuil - Site BTS Mahgreb Arabe - Oujda</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<?php	echo $this->Html->css('style');?>
<?php	echo $this->Html->script('jquery-1.7.1.min');?>
<?php	echo $this->Html->script('superfish');?> 
<?php	echo $this->Html->script('jquery.easing.1.3');?>


<?php	echo $this->Html->script('jquery.cycle.all.min');?>

<script type="text/javascript">
$(function(){
$('.mp-slider').cycle({
pager: '#mp-pags',
timeout:7000,
pagerAnchorBuilder: pagerFactory
});
function pagerFactory(index, slide) {
var titles = []
titles[0] = $("#mp-pags-cont li:eq(0)").text()
titles[1] = $("#mp-pags-cont li:eq(1)").text()
titles[2] = $("#mp-pags-cont li:eq(2)").text()
return '<li><a href="#">'+(titles[index])+'</a></li>';
}; 
})
</script>
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
<img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
</a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<!--[if lt IE 9]>
<?php	echo $this->Html->script('html5');?>
<?php	echo $this->Html->css('ie');?>
<![endif]-->
</head>
<body id="page1" >
<!-- Header -->
<header>
    <div class="inner">
        <?php echo $this->Html->image("bts.png",array("style"=>"left: -200px;position: relative;"));?>
	</div>

    <nav>
        <ul class="sf-menu">
            <li><a href="<?php echo $this->Html->url('/') ?>">acceuil</a></li>
            <li><a href="about.html">apropos de nous</a>
            <ul>
                    <li><a href="<?php echo $this->Html->url('/contactez-nous') ?>">Présentation lycée</a></li>
                    <li><a href="<?php echo $this->Html->url('/presentation_bts') ?>">Présentation BTS</a></li>
                    <li><a href="<?php echo $this->Html->url('/conditionacces') ?>">conditions </a></li>
                    <li><a href="<?php echo $this->Html->url('/Reglement') ?>"> réglement </a></li>
                </ul>
<li>

<a href="services.html">BTS OUJDA</a>
<ul>
<li><a href="#">Nos Missions</a></li>
<li><a href="#">Nos Services</a></li>
<li>

<a href="#">Nos Filières</a>
<ul>
<li><a href="#">Développement des Systèmes d'Information</a></li>
<li><a href="#">Systèmes et Réseaux Informatiques</a></li>
<li><a href="#">Electromecanique et Systeme Automatisé</a></li>
</ul>
</li>
<li><a href="/nos-infos">Nos Informations</a></li>
<li><a href="#">Nos Annoces</a></li>
</ul>

        
            <li><a href="contacts.html">contacts</a>
            <ul>
                    <li><a href="<?php echo $this->Html->url('/contactez-nous') ?>">Nous envoyer un message</a></li>
                    <li><a href="<?php echo $this->Html->url('/nos-coordonees') ?>">Nos Coordonées</a></li>
                    <li><a href="<?php echo $this->Html->url('/etineraire') ?>">Etinéraire</a></li>
                    <li><a href="<?php echo $this->Html->url('/notre-localisation') ?>">Notre Localisation</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $this->Html->url('/admin') ?>">Se connecter</a></li>
        </ul>
        <div class="clear"></div>
    </nav>
<div class="slider-container">
<div class="mp-slider">
<div><?php echo $this->Html->image("slide-1.png");?><a href="#">read more</a></div>
<div><?php echo $this->Html->image("slide-2.png");?><a href="#">read more</a></div>
<div><?php echo $this->Html->image("slide-3.png");?><a href="#">read more</a></div>
</div>
</div>
</header>
<!-- Content -->
<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - August 06, 2011!</div>

<div class="container_24">
<div class="grid_24 content-border">
<div class="top-content-box">
<ul id="mp-pags-cont-extra">
<li><a href="#">Stratégies d'études</a></li>
<li><a href="#">Activités Récentes</a></li>
<li><a href="#">Perspectives et Future</a></li>
</ul>
</div>
<div class="top-row">
<h1 class="aligncenter">Votre Future, Votre Carrière <br> Notre Résponsabilité.</h1>
<div class="wrapper">
<div class="grid_5 prefix_1 alpha">
<div class="wrapper">
<span class="dropcap color-1">A.</span>
<div class="extra-wrap">
<strong class="str-1 color-1">
DSI :
Développement des Systèmes d’Information

</strong>

</div>
</div>
</div>
<div class="grid_5">
<span class="dropcap color-2">B.</span>
<div class="extra-wrap">
<strong class="str-1 color-2">
SRI :
Systèmes et Réseaux Informatiques

</strong>

</div>
</div>
<div class="grid_5">
<span class="dropcap color-3">C.</span>
<div class="extra-wrap">
<strong class="str-1 color-3">
ESA :
Electromécanique et Systèmes Automatisés

</strong>

</div>
</div>
<div class="grid_5 suffix_1 omega">
<span class="dropcap color-4">D.</span>
<div class="extra-wrap">
<strong class="str-1 color-4">
CG :
Comptabilité et Gestion 

</strong>

</div>
</div>
</div>
</div>
<div class="wrapper indent-top">
<article class="grid_15 suffix_1 prefix_1 alpha">
<h2>Bienvenu</h2>
<p>
<div class="wrapper">
<div class="grid_5 alpha">
<figure class="img-indent-bot">

<?php echo $this->Html->image("page1-img1.jpg");?>
</figure>
<div class="alignright author">
ADIL MANI
<em>manager</em>
</div>
</div>
<div class="grid_5">
<figure class="img-indent-bot">

<?php echo $this->Html->image("page1-img2.jpg");?>
</figure>
<div class="alignright author">
Thom Smith
<em>marketolog</em>
</div>
</div>
<div class="grid_5 omega">
<figure class="img-indent-bot">
<?php echo $this->Html->image("page1-img3.jpg");?>
</figure>
<div class="alignright author">
April Black
<em>manager</em>
</div>
</div>
</div>
</article>
<article class="grid_6 suffix_1 omega">
<h2>Dernières nouvelles:</h2>
<dl class="news-list">
<dt>
<a href="#">Nov. <strong>25</strong>, 2012</a></dt>
<dd>
The PSD source files of this Point template are available for free for the registered members of TemplateMonster.com. Feel free to get them! 
</dd>
<dt>
<a href="#">Nov. <strong>20</strong>, 2012</a>
</dt>
<dd class="last">
This website template has several pages: Home, About, Services, Products, Contacts (note that contact us form - doesn't work)
</dd>
</dl>
<div class="alignright">
<a href="#" class="link">read more</a>
</div>
</article>
</div>
</div>
<div class="clear"></div>
</div>
    <div class="container_24">
    	<div class="grid_24 content-border">
                       <div class="top-content-box">
                <ul id="mp-pags-cont-extra">
                    <li><a href="#">Stratégies d'études</a></li>
                    <li><a href="#">Activités Récentes</a></li>
                    <li><a href="#">Perspectives et Future</a></li>
                </ul>
            </div>
            <div class="top-row">
            	<h1 class="aligncenter">Votre Future, Votre Carrière <br> Notre Résponsabilité.</h1>
                <div class="wrapper">
                	<div class="grid_5 prefix_1 alpha">
                    	<div class="wrapper">
                        	<span class="dropcap color-1">A.</span>
                            <div class="extra-wrap">
                            	<strong class="str-1 color-1">
                                 DSI :
                                Développement des Systèmes d’Information
                               
                                </strong>
                                
                            </div>
                        </div>
                    </div>
                    <div class="grid_5">
                    	<span class="dropcap color-2">B.</span>
                            <div class="extra-wrap">
                            <strong class="str-1 color-2">
                             SRI :
                              Systèmes et Réseaux Informatiques
                             
                            </strong>
                           
                        </div>
                    </div>
                    <div class="grid_5">
                    	<span class="dropcap color-3">C.</span>
                        <div class="extra-wrap">
                            <strong class="str-1 color-3">
                             ESA :
                             Electromécanique et Systèmes Automatisés
                            
                            </strong>
                          
                        </div>
                    </div>
                    <div class="grid_5 suffix_1 omega">
                    	<span class="dropcap color-4">D.</span>
                        <div class="extra-wrap">
                            <strong class="str-1 color-4">
                            CG :
                            Comptabilité et Gestion 
                                                  
                              </strong>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper indent-top">
            	<article class="grid_15 suffix_1 prefix_1 alpha">
                	<h2>Bienvenu</h2>
                    <p>
                               <div class="wrapper">
                    	<div class="grid_5 alpha">
                        	<figure class="img-indent-bot">
                            
                                <?php echo $this->Html->image("page1-img1.jpg");?>
                            </figure>
                            <div class="alignright author">
                            	ADIL MANI
                                <em>manager</em>
                            </div>
                        </div>
                        <div class="grid_5">
                        	<figure class="img-indent-bot">
                            	
                                <?php echo $this->Html->image("page1-img2.jpg");?>
                            </figure>
                            <div class="alignright author">
                            	Thom Smith
                                <em>marketolog</em>
                            </div>
                        </div>
                        <div class="grid_5 omega">
                        	<figure class="img-indent-bot">
                            <?php echo $this->Html->image("page1-img3.jpg");?>
                            </figure>
                            <div class="alignright author">
                            	April Black
                                <em>manager</em>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="grid_6 suffix_1 omega">
                	<h2>Dernières nouvelles:</h2>
                    <dl class="news-list">
                    	<dt>
                        	<a href="#">Nov. <strong>25</strong>, 2012</a></dt>
                        <dd>
                        	The PSD source files of this Point template are available for free for the registered members of TemplateMonster.com. Feel free to get them! 
                        </dd>
                        <dt>
                        	<a href="#">Nov. <strong>20</strong>, 2012</a>
                        </dt>
                        <dd class="last">
                        	This website template has several pages:  Home, About, Services, Products, Contacts (note that contact us form - doesn't work)
                        </dd>
                    </dl>
                    <div class="alignright">
                        <a href="#" class="link">read more</a>
                    </div>
                </article>
            </div>
        </div>
        <div class="clear"></div>
    </div>

</section>
<aside>
<div class="container_24">
<div class="wrapper">
<div class="grid_24 aside-bg">
<div class="wrapper">
<article class="grid_5 prefix_1 alpha">
<h4>Services:</h4>
<ul class="list-1">
<li><a href="#">Vivamus hendrerit mauris</a></li> 
<li><a href="#">ut dui gravida ut viverra</a></li> 
<li><a href="#">lectus tincidunt Cras</a></li> 
<li><a href="#">mattis tempor eros nec</a></li>
</ul>
</article>
<article class="grid_5">
<h4>Solutions:</h4>
<ul class="list-1">
<li><a href="#">lectus tincidunt Cras</a></li>
<li><a href="#">mattis tempor eros nec</a></li> 
<li><a href="#">tristique Sed sed felis</a></li>
<li><a href="#">arcu vel vehicula augue</a></li>
</ul>
</article>
<article class="grid_5">
<h4>Programs:</h4>
<ul class="list-1">
<li><a href="#">mattis tempor eros nec</a></li> 
<li><a href="#">tristique Sed sed felis</a></li> 
<li><a href="#">arcu vel vehicula augue</a></li>
<li><a href="#">Maecenas faucibus </a></li>
</ul>
</article>
<article class="grid_8 omega">
<h4 class="pad-1">Testimonials:</h4>
<blockquote class="quote">
<div class="inner">
Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec tristique. Sed sed felis arcu, vel vehicula augue.
</div>
</blockquote>
<div class="author alignright pad-2">
April Black <span>( manager )</span>
</div>
</article>
</div>
</div>
</div>
</div>
</aside>
<!-- Footer -->
<footer>

<div class="copyright">
&copy; 2012 <strong class="footer-logo">Pet clinic</strong>

</div>
<ul class="social-list">
<li><a href="#"><?php echo $this->Html->image("social-link-1.jpg");?></a></li>
<li><a href="#"><?php echo $this->Html->image("social-link-2.jpg");?></a></li>
<li><a href="#"><?php echo $this->Html->image("social-link-3.jpg");?></a></li>
</ul>

</footer>
</body>
</html>
