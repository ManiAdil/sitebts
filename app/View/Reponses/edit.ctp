<div class="reponses form">
<?php echo $this->Form->create('Reponse'); ?>
	<fieldset>
		<legend><?php echo __('Edit Reponse'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('contenu');
		echo $this->Form->input('question_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Reponse.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Reponse.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Reponses'), array('action' => 'index')); ?></li>
	</ul>
</div>
