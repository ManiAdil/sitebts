
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Réponses</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"add","admin"=>true));?>">Ajouter une réponse</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des reponses</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>contenu</th>
<th>question_id</th>
<th>Actions</th>
</tr>


<?php foreach ($reponses as $reponse): ?>
	<tr>
		<td><?php echo h($reponse['Reponse']['id']); ?>&nbsp;</td>
		<td><?php echo h($reponse['Reponse']['contenu']); ?>&nbsp;</td>
		<td><?php echo h($reponse['Reponse']['question_id']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $reponse['Reponse']['id'])); ?>|
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $reponse['Reponse']['id']), null, __('Vous êtes sûr de vouloir supprimer element avec ID :# %s?', $reponse['Reponse']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>