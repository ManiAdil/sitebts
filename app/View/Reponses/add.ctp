<div class="reponses form">
<?php echo $this->Form->create('Reponse'); ?>
	<fieldset>
		<legend><?php echo __('Add Reponse'); ?></legend>
	<?php
		echo $this->Form->input('contenu');
		echo $this->Form->input('question_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Reponses'), array('action' => 'index')); ?></li>
	</ul>
</div>
