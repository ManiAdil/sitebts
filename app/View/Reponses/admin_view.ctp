<div class="reponse view">
<h2><?php  echo __('Reponse'); ?></h2>
	<dl>
		<dt><?php echo __('id'); ?></dt>
		<dd>
			<?php echo h($reponse['Reponse']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('contenu'); ?></dt>
		<dd>
			<?php echo h($reponse['Reponse']['contenu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('question_id'); ?></dt>
		<dd>
			<?php echo h($reponse['Reponse']['question_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Reponse'), array('action' => 'edit', $reponse['Reponse']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Reponse'), array('action' => 'delete', $reponse['Reponse']['id']), null, __('Are you sure you want to delete # %s?', $reponse['Reponse']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Reponses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reponse'), array('action' => 'add')); ?> </li>
	</ul>
</div>
