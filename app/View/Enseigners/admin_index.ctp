
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste </a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Enseigners","action"=>"add","admin"=>true));?>">Ajouter </a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Classe</th>
<th>Enseignant</th>
<th>Action</th>
</tr>
<?php foreach ($enseigners as $enseigner): ?>
	<tr>
		<td><?php echo h($enseigner['Enseigner']['id']); ?>&nbsp;</td>
        <td><?php echo h($enseigner['Classee']['nom']); ?>&nbsp;</td>
        <td><?php echo h($enseigner['Enseignant']['nom']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $enseigner['Enseigner']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $enseigner['Enseigner']['id']), null, __('Are you sure you want to delete # %s?', $enseigner['Enseigner']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>