<div class="enseigners form">
<?php echo $this->Form->create('Enseigner'); ?>
	<fieldset>
		<legend><?php echo __('Edit Enseigner'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('classee_id');
		echo $this->Form->input('enseignant_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Enseigner.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Enseigner.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Enseigners'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
	</ul>
</div>
