<div class="enseigners index">
	<h2><?php echo __('Enseigners'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('classee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('enseignant_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($enseigners as $enseigner): ?>
	<tr>
		<td><?php echo h($enseigner['Enseigner']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($enseigner['Classee']['nom'], array('controller' => 'classees', 'action' => 'view', $enseigner['Classee']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($enseigner['Enseignant']['nom'], array('controller' => 'enseignants', 'action' => 'view', $enseigner['Enseignant']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $enseigner['Enseigner']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $enseigner['Enseigner']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $enseigner['Enseigner']['id']), null, __('Are you sure you want to delete # %s?', $enseigner['Enseigner']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Enseigner'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
	</ul>
</div>
