<div class="enseigners view">
<h2><?php  echo __('Enseigner'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($enseigner['Enseigner']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($enseigner['Classee']['nom'], array('controller' => 'classees', 'action' => 'view', $enseigner['Classee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enseignant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($enseigner['Enseignant']['nom'], array('controller' => 'enseignants', 'action' => 'view', $enseigner['Enseignant']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Enseigner'), array('action' => 'edit', $enseigner['Enseigner']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Enseigner'), array('action' => 'delete', $enseigner['Enseigner']['id']), null, __('Are you sure you want to delete # %s?', $enseigner['Enseigner']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseigners'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseigner'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
	</ul>
</div>
