
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Sitesamis</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"sitesamis","action"=>"add","admin"=>true));?>">Ajouter un nouveau site</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des sites amis</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>libelle</th>
<th>URL</th>

</tr>
<?php foreach ($sitesamis as $sitesami): ?>
	<tr>
		<td><?php echo h($sitesami['Sitesami']['id']); ?>&nbsp;</td>
		<td><?php echo h($sitesami['Sitesami']['libelle']); ?>&nbsp;</td>
        	<td><?php echo h($sitesami['Sitesami']['URL']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $sitesami['Sitesami']['id'])); ?>|
				<?php echo $this->Form->postLink(__('supprimer '), array('action' => 'delete', $sitesami['Sitesami']['id']), null, __('Are you sure you want to delete # %s?', $sitesami['Sitesami']['id'])); ?>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>