<div class="sitesamis form">
<?php echo $this->Form->create('Sitesami'); ?>
	<fieldset>
		<legend><?php echo __('Add Sitesami'); ?></legend>
	<?php
		echo $this->Form->input('libelle');
		echo $this->Form->input('URL');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sitesamis'), array('action' => 'index')); ?></li>
	</ul>
</div>
