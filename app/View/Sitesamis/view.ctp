<div class="sitesamis view">
<h2><?php  echo __('Sitesami'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($sitesami['Sitesami']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($sitesami['Sitesami']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('URL'); ?></dt>
		<dd>
			<?php echo h($sitesami['Sitesami']['URL']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sitesami'), array('action' => 'edit', $sitesami['Sitesami']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sitesami'), array('action' => 'delete', $sitesami['Sitesami']['id']), null, __('Are you sure you want to delete # %s?', $sitesami['Sitesami']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sitesamis'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sitesami'), array('action' => 'add')); ?> </li>
	</ul>
</div>
