<div class="coordones view">
<h2><?php  echo __('Coordone'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fix'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Fix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Adresse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($coordone['Coordone']['Email']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Coordone'), array('action' => 'edit', $coordone['Coordone']['Id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Coordone'), array('action' => 'delete', $coordone['Coordone']['Id']), null, __('Are you sure you want to delete # %s?', $coordone['Coordone']['Id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Coordones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coordone'), array('action' => 'add')); ?> </li>
	</ul>
</div>
