
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Coordonés</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste des Coordonés</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"coordones","action"=>"add","admin"=>true));?>">Ajouter un nouveau coordones</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des coordones</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Tel</th>
<th>Fix</th>
<th>Fax</th>
<th>Adresse</th>
<th>Email</th>
<th>Actions</th>
</tr>
<?php foreach ($coordones as $coordone): ?>
	<tr>
		<td><?php echo h($coordone['Coordone']['Id']); ?>&nbsp;</td>
		<td><?php echo h($coordone['Coordone']['Tel']); ?>&nbsp;</td>
		<td><?php echo h($coordone['Coordone']['Fix']); ?>&nbsp;</td>
		<td><?php echo h($coordone['Coordone']['Fax']); ?>&nbsp;</td>
		<td><?php echo h($coordone['Coordone']['Adresse']); ?>&nbsp;</td>
		<td><?php echo h($coordone['Coordone']['Email']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $coordone['Coordone']['Id'])); ?>|
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $coordone['Coordone']['Id']), null, __('Vous êtes sûr de vouloir supprimer element avec ID :# %s?', $coordone['Coordone']['Id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>