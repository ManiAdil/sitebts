<div class="coordones form">
<?php echo $this->Form->create('Coordone'); ?>
	<fieldset>
		<legend><?php echo __('Add Coordone'); ?></legend>
	<?php
		echo $this->Form->input('Tel');
		echo $this->Form->input('Fix');
		echo $this->Form->input('Fax');
		echo $this->Form->input('Adresse');
		echo $this->Form->input('Email');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Coordones'), array('action' => 'index')); ?></li>
	</ul>
</div>
