<div class="classees form">
<?php echo $this->Form->create('Classee'); ?>
	<fieldset>
		<legend><?php echo __('Add Classee'); ?></legend>
	<?php
		echo $this->Form->input('nom');
		echo $this->Form->input('filiere_id');
		echo $this->Form->input('niveau_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Classees'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Filieres'), array('controller' => 'filieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiere'), array('controller' => 'filieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('controller' => 'niveaus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Niveau'), array('controller' => 'niveaus', 'action' => 'add')); ?> </li>
	</ul>
</div>
