
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Classees</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Modifier</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>modifier une classe</h3>
						</div>
<?php echo $this->Form->create('Classee',array("type"=>"file")); ?>
<table cellpadding="20px">
<tr>
<td colspan="2"> <?php echo $this->Form->input('id',array('label'=>''));?> </td>
</tr>
<tr>
<td>nom : </td>
<td>	<?php echo $this->Form->input('nom',array('label'=>''));?></td>
</tr>
<tr>
<td>filiere : </td>
<td>	<?php echo $this->Form->input('filiere_id',array('label'=>''));?></td>
</tr>
<td>niveau : </td>
<td>	<?php echo $this->Form->input('niveau_id',array('label'=>''));?></td>
</tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Classees","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>

