<div class="classees view">
<h2><?php  echo __('Classee'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($classee['Classee']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($classee['Classee']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($classee['Filiere']['id'], array('controller' => 'filieres', 'action' => 'view', $classee['Filiere']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Niveau'); ?></dt>
		<dd>
			<?php echo $this->Html->link($classee['Niveau']['id'], array('controller' => 'niveaus', 'action' => 'view', $classee['Niveau']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Classee'), array('action' => 'edit', $classee['Classee']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Classee'), array('action' => 'delete', $classee['Classee']['id']), null, __('Are you sure you want to delete # %s?', $classee['Classee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Filieres'), array('controller' => 'filieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiere'), array('controller' => 'filieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('controller' => 'niveaus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Niveau'), array('controller' => 'niveaus', 'action' => 'add')); ?> </li>
	</ul>
</div>
