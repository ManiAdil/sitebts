<div class="classees form">
<?php echo $this->Form->create('Classee'); ?>
	<fieldset>
		<legend><?php echo __('Edit Classee'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nom');
		echo $this->Form->input('filiere_id');
		echo $this->Form->input('niveau_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Classee.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Classee.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Classees'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Filieres'), array('controller' => 'filieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiere'), array('controller' => 'filieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('controller' => 'niveaus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Niveau'), array('controller' => 'niveaus', 'action' => 'add')); ?> </li>
	</ul>
</div>
