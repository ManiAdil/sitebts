<div class="classees index">
	<h2><?php echo __('Classees'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th><?php echo $this->Paginator->sort('filiere_id'); ?></th>
			<th><?php echo $this->Paginator->sort('niveau_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($classees as $classee): ?>
	<tr>
		<td><?php echo h($classee['Classee']['id']); ?>&nbsp;</td>
		<td><?php echo h($classee['Classee']['nom']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($classee['Filiere']['id'], array('controller' => 'filieres', 'action' => 'view', $classee['Filiere']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($classee['Niveau']['id'], array('controller' => 'niveaus', 'action' => 'view', $classee['Niveau']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $classee['Classee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $classee['Classee']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $classee['Classee']['id']), null, __('Are you sure you want to delete # %s?', $classee['Classee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Classee'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Filieres'), array('controller' => 'filieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiere'), array('controller' => 'filieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Niveaus'), array('controller' => 'niveaus', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Niveau'), array('controller' => 'niveaus', 'action' => 'add')); ?> </li>
	</ul>
</div>
