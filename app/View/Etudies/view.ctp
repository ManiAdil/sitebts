<div class="etudies view">
<h2><?php  echo __('Etudy'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($etudy['Etudy']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Matiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($etudy['Matiere']['id'], array('controller' => 'matieres', 'action' => 'view', $etudy['Matiere']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($etudy['Classee']['nom'], array('controller' => 'classees', 'action' => 'view', $etudy['Classee']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Etudy'), array('action' => 'edit', $etudy['Etudy']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Etudy'), array('action' => 'delete', $etudy['Etudy']['id']), null, __('Are you sure you want to delete # %s?', $etudy['Etudy']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudies'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudy'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
