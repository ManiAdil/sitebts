<div class="etudies index">
	<h2><?php echo __('Etudies'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('matiere_id'); ?></th>
			<th><?php echo $this->Paginator->sort('classee_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($etudies as $etudy): ?>
	<tr>
		<td><?php echo h($etudy['Etudy']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($etudy['Matiere']['id'], array('controller' => 'matieres', 'action' => 'view', $etudy['Matiere']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($etudy['Classee']['nom'], array('controller' => 'classees', 'action' => 'view', $etudy['Classee']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $etudy['Etudy']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $etudy['Etudy']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $etudy['Etudy']['id']), null, __('Are you sure you want to delete # %s?', $etudy['Etudy']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Etudy'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
