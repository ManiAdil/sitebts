<div class="etudies form">
<?php echo $this->Form->create('Etudy'); ?>
	<fieldset>
		<legend><?php echo __('Add Etudy'); ?></legend>
	<?php
		echo $this->Form->input('matiere_id');
		echo $this->Form->input('classee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Etudies'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
