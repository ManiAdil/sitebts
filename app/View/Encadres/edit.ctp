<div class="encadres form">
<?php echo $this->Form->create('Encadre'); ?>
	<fieldset>
		<legend><?php echo __('Edit Encadre'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('matiere_id');
		echo $this->Form->input('enseignant_id');
		echo $this->Form->input('classee_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Encadre.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Encadre.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
