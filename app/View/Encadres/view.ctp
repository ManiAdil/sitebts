<div class="encadres view">
<h2><?php  echo __('Encadre'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($encadre['Encadre']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Matiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encadre['Matiere']['id'], array('controller' => 'matieres', 'action' => 'view', $encadre['Matiere']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enseignant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encadre['Enseignant']['nom'], array('controller' => 'enseignants', 'action' => 'view', $encadre['Enseignant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($encadre['Classee']['nom'], array('controller' => 'classees', 'action' => 'view', $encadre['Classee']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Encadre'), array('action' => 'edit', $encadre['Encadre']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Encadre'), array('action' => 'delete', $encadre['Encadre']['id']), null, __('Are you sure you want to delete # %s?', $encadre['Encadre']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
	</ul>
</div>
