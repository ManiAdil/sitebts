
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste </a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Encadres","action"=>"add","admin"=>true));?>">Ajouter </a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste des </h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Matiére</th>
<th>Enseignant</th>
<th>Classe</th>
<th>Action</th>
</tr>
<?php foreach ($encadres as $encadre): ?>
	<tr>
		<td><?php echo h($encadre['Encadre']['id']); ?>&nbsp;</td>
        <td><?php echo h($encadre['Matiere']['nom']); ?>&nbsp;</td>
        <td><?php echo h($encadre['Enseignant']['nom']); ?>&nbsp;</td>
        <td><?php echo h($encadre['Classee']['nom']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $encadre['Encadre']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $encadre['Encadre']['id']), null, __('Are you sure you want to delete # %s?', $encadre['Encadre']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>