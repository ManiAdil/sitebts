<div class="missions form">
<?php echo $this->Form->create('Mission'); ?>
	<fieldset>
		<legend><?php echo __('Add Mission'); ?></legend>
	<?php
		echo $this->Form->input('titre');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Missions'), array('action' => 'index')); ?></li>
	</ul>
</div>
