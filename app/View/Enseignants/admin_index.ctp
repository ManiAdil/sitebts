
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste des enseignants</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Enseignants","action"=>"add","admin"=>true));?>">Ajouter un nouveau enseignant</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste des enseignants</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Photo</th>
<th>Nom</th>
<th>Prénom</th>
<th>Email</th>
<th>Tel</th>
<th>Action</th>
</tr>
<?php foreach ($enseignants as $enseignant): ?>
	<tr>
		<td><?php echo h($enseignant['Enseignant']['id']); ?>&nbsp;
		<td><?php echo $this->Html->image("Upload/".$enseignant["Enseignant"]["image"],array("height"=>"280","width"=>"100"));?></td>
        <td><?php echo h($enseignant['Enseignant']['nom']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['prenom']); ?>&nbsp;</td>
        <td><?php echo "<a href=mailto:".$enseignant['Enseignant']['email'].">".$enseignant['Enseignant']['email']."</a>"; ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['tel']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $enseignant['Enseignant']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $enseignant['Enseignant']['id']), null, __('Are you sure you want to delete # %s?', $enseignant['Enseignant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>