<div class="enseignants view">
<h2><?php  echo __('Enseignant'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prenom'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['prenom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['adresse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ville'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['ville']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($enseignant['Enseignant']['tel']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Enseignant'), array('action' => 'edit', $enseignant['Enseignant']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Enseignant'), array('action' => 'delete', $enseignant['Enseignant']['id']), null, __('Are you sure you want to delete # %s?', $enseignant['Enseignant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Affectations'), array('controller' => 'affectations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Affectation'), array('controller' => 'affectations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseigners'), array('controller' => 'enseigners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseigner'), array('controller' => 'enseigners', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Affectations'); ?></h3>
	<?php if (!empty($enseignant['Affectation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Classe Id'); ?></th>
		<th><?php echo __('Enseignant Id'); ?></th>
		<th><?php echo __('Date Debut'); ?></th>
		<th><?php echo __('Date Fin'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($enseignant['Affectation'] as $affectation): ?>
		<tr>
			<td><?php echo $affectation['id']; ?></td>
			<td><?php echo $affectation['classe_id']; ?></td>
			<td><?php echo $affectation['enseignant_id']; ?></td>
			<td><?php echo $affectation['date debut']; ?></td>
			<td><?php echo $affectation['date fin']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'affectations', 'action' => 'view', $affectation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'affectations', 'action' => 'edit', $affectation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'affectations', 'action' => 'delete', $affectation['id']), null, __('Are you sure you want to delete # %s?', $affectation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Affectation'), array('controller' => 'affectations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Encadres'); ?></h3>
	<?php if (!empty($enseignant['Encadre'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Matiere Id'); ?></th>
		<th><?php echo __('Enseignant Id'); ?></th>
		<th><?php echo __('Classe Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($enseignant['Encadre'] as $encadre): ?>
		<tr>
			<td><?php echo $encadre['id']; ?></td>
			<td><?php echo $encadre['matiere_id']; ?></td>
			<td><?php echo $encadre['enseignant_id']; ?></td>
			<td><?php echo $encadre['classe_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'encadres', 'action' => 'view', $encadre['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'encadres', 'action' => 'edit', $encadre['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'encadres', 'action' => 'delete', $encadre['id']), null, __('Are you sure you want to delete # %s?', $encadre['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Enseigners'); ?></h3>
	<?php if (!empty($enseignant['Enseigner'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Classe Id'); ?></th>
		<th><?php echo __('Enseignant Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($enseignant['Enseigner'] as $enseigner): ?>
		<tr>
			<td><?php echo $enseigner['id']; ?></td>
			<td><?php echo $enseigner['classe_id']; ?></td>
			<td><?php echo $enseigner['enseignant_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'enseigners', 'action' => 'view', $enseigner['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'enseigners', 'action' => 'edit', $enseigner['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'enseigners', 'action' => 'delete', $enseigner['id']), null, __('Are you sure you want to delete # %s?', $enseigner['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Enseigner'), array('controller' => 'enseigners', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
