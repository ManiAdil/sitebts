<div class="enseignants form">
<?php echo $this->Form->create('Enseignant',array("type"=>"file")); ?>
	<fieldset>
		<legend><?php echo __('Add Enseignant'); ?></legend>
	<?php
		echo $this->Form->input('nom');
		echo $this->Form->input('prenom');
		echo $this->Form->input('adresse');
		echo $this->Form->input('ville');
		echo $this->Form->input('email');
		echo $this->Form->input('image');
		echo $this->Form->input('tel');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Enseignants'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Affectations'), array('controller' => 'affectations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Affectation'), array('controller' => 'affectations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseigners'), array('controller' => 'enseigners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseigner'), array('controller' => 'enseigners', 'action' => 'add')); ?> </li>
	</ul>
</div>
