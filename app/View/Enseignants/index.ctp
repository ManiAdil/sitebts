<div class="enseignants index">
	<h2><?php echo __('Enseignants'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th><?php echo $this->Paginator->sort('prenom'); ?></th>
			<th><?php echo $this->Paginator->sort('adresse'); ?></th>
			<th><?php echo $this->Paginator->sort('ville'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('tel'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($enseignants as $enseignant): ?>
	<tr>
		<td><?php echo h($enseignant['Enseignant']['id']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['nom']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['prenom']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['adresse']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['ville']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['email']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['image']); ?>&nbsp;</td>
		<td><?php echo h($enseignant['Enseignant']['tel']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $enseignant['Enseignant']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $enseignant['Enseignant']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $enseignant['Enseignant']['id']), null, __('Are you sure you want to delete # %s?', $enseignant['Enseignant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Affectations'), array('controller' => 'affectations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Affectation'), array('controller' => 'affectations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Encadres'), array('controller' => 'encadres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Encadre'), array('controller' => 'encadres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseigners'), array('controller' => 'enseigners', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseigner'), array('controller' => 'enseigners', 'action' => 'add')); ?> </li>
	</ul>
</div>
