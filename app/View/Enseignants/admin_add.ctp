
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Enseignants</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Ajouter</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Ajouter un enseignant</h3>
						</div>
<?php echo $this->Form->create('Enseignant',array("type"=>"file")); ?>
<table cellpadding="20px">
<tr>
<td>nom : </td>
<td>	<?php echo $this->Form->input('nom',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>prenom : </td>
<td>	<?php echo $this->Form->input('prenom',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>adresse : </td>
<td>	<?php echo $this->Form->input('adresse',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>ville : </td>
<td>	<?php echo $this->Form->input('ville',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>email : </td>
<td>	<?php echo $this->Form->input('email',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>image : </td>
<td>	<?php echo $this->Form->input('imagefile',array('type'=>'file'));?></td>
</tr>
<tr>
<tr>
<td>tel: </td>
<td>	<?php echo $this->Form->input('tel',array('label'=>''));?></td>
</tr>
<tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Enseignants","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>

