<div class="messages view">
<h2><?php  echo __('Message'); ?></h2>
	<dl>
		<dt><?php echo __('id'); ?></dt>
		<dd>
			<?php echo h($message['Message']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('nom'); ?></dt>
		<dd>
			<?php echo h($message['Message']['nom']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('email'); ?></dt>
		<dd>
			<?php echo h($message['Message']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('contenu'); ?></dt>
		<dd>
			<?php echo h($message['Message']['contenu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('lu'); ?></dt>
		<dd>
			<?php echo h($message['Message']['lu']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Message'), array('action' => 'edit', $message['Message']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Message'), array('action' => 'delete', $message['Message']['id']), null, __('Are you sure you want to delete # %s?', $message['Message']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Messages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Message'), array('action' => 'add')); ?> </li>
	</ul>
</div>
