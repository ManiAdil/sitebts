
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Messages</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste Messages</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"messages","action"=>"add","admin"=>true));?>">Envoyé un nouveau Message</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des Messages</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>nom</th>
<th>email</th>
<th>contenu</th>
<th>Actions</th>

</tr>
<?php foreach ($messages as $message): ?>
<tr>
<?php if ($message['Message']['lu']==0)
{?>
    	<td style="background-color: #ea758a;"><?php echo h($message['Message']['id']); ?>&nbsp;</td>
		<td style="background-color: #ea758a;"><?php echo h($message['Message']['nom']); ?>&nbsp;</td>
		<td style="background-color: #ea758a;"><?php echo h($message['Message']['email']); ?>&nbsp;</td>
		<td style="background-color: #ea758a;"><?php echo h($message['Message']['contenu']); ?>&nbsp;</td>
	
		
		<td class="actions" style="background-color: #ea758a;">
	
			<?php echo $this->Html->link(__('Afficher'), array('action' => 'view', $message['Message']['id'])); ?>|
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $message['Message']['id']), null, __('Vous êtes sûr de vouloir supprimer element avec ID :# %s?', $message['Message']['id'])); ?>
		</td>
<?php }else
{ ?>
       	<td style="background-color: #52e488;"><?php echo h($message['Message']['id']); ?>&nbsp;</td>
		<td style="background-color: #52e488;"><?php echo h($message['Message']['nom']); ?>&nbsp;</td>
		<td style="background-color: #52e488;"><?php echo h($message['Message']['email']); ?>&nbsp;</td>
		<td style="background-color: #52e488;"><?php echo h($message['Message']['contenu']); ?>&nbsp;</td>
	
		
		<td class="actions" style="background-color: #52e488;">
	
			<?php echo $this->Html->link(__('Afficher'), array('action' => 'view', $message['Message']['id'])); ?>|
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $message['Message']['id']), null, __('Vous êtes sûr de vouloir supprimer element avec ID :# %s?', $message['Message']['id'])); ?>
		</td>
    
<?php }?>

	
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>