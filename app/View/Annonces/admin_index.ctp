
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Annonces</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"annonces","action"=>"add","admin"=>true));?>">Ajouter une nouvelle annonce</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des annonces</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th> </th>
<th>Titre</th>
<th>Actions</th>
</tr>
<?php foreach ($annonces as $annonce): ?>
	<tr>
		<td><?php echo h($annonce['Annonce']['id']); ?>&nbsp;</td>
		<td><?php echo h($annonce['Annonce']['libelle']); ?>&nbsp;</td>
        	<td><?php echo h($annonce['Annonce']['description']); ?>&nbsp;</td>
        
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $annonce['Annonce']['id'])); ?>|
				<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $annonce['Annonce']['id']), null, __('etes vous sur de vouloir supprimer cette annonce ?# %s?', $annonce['Annonce']['id'])); ?>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>