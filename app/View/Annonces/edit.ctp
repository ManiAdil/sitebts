<div class="annonces form">
<?php echo $this->Form->create('Annonce'); ?>
	<fieldset>
		<legend><?php echo __('Edit Annonce'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('libelle');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Annonce.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Annonce.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Annonces'), array('action' => 'index')); ?></li>
	</ul>
</div>
