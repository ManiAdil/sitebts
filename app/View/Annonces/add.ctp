<div class="annonces form">
<?php echo $this->Form->create('Annonce'); ?>
	<fieldset>
		<legend><?php echo __('Add Annonce'); ?></legend>
	<?php
		echo $this->Form->input('libelle');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Annonces'), array('action' => 'index')); ?></li>
	</ul>
</div>
