<div class="annonces form">
<?php echo $this->Form->create('Annonce'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Annonce'); ?></legend>
	<?php
		echo $this->Form->input('libelle');
	?>
    <?php
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Annonces'), array('action' => 'index')); ?></li>
	</ul>
</div>
