<div class="annonces view">
<h2><?php  echo __('Annonce'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($annonce['Annonce']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($annonce['Annonce']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($annonce['Annonce']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($annonce['Annonce']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Annonce'), array('action' => 'edit', $annonce['Annonce']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Annonce'), array('action' => 'delete', $annonce['Annonce']['id']), null, __('Are you sure you want to delete # %s?', $annonce['Annonce']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Annonces'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Annonce'), array('action' => 'add')); ?> </li>
	</ul>
</div>
