
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">etudiants</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Ajouter</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Ajouter une etudiant</h3>
						</div>
<?php echo $this->Form->create('Etudiant',array("type"=>"file")); ?>
<table cellpadding="20px">
<td>Nom de la etudiant : </td>
<td>	<?php echo $this->Form->input('nom',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>Prenom de la etudiant : </td>
<td>	<?php echo $this->Form->input('prenom',array('label'=>''));?></td>
</tr>
<tr>
<td>Adresse de la etudiant : </td>
<td>	<?php echo $this->Form->input('adresse',array('label'=>''));?></td>
</tr>
<tr>
<td>Tel de la etudiant : </td>
<td>	<?php echo $this->Form->input('tel',array('label'=>''));?></td>
</tr>
<tr>
<td>CNE de la etudiant : </td>
<td>	<?php echo $this->Form->input('cin',array('label'=>''));?></td>
</tr>
<tr>
<td>E-mail de la etudiant : </td>
<td>	<?php echo $this->Form->input('email',array('label'=>''));?></td>
</tr>
<tr>
<td>Sexe de la etudiant : </td>
<td>	<?php echo $this->Form->input('sexe',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<td>image : </td>
<td>	<?php echo $this->Form->input('imagefile',array('type'=>'file'));?></td>
</tr>
<td>Filière : </td>
<td>	<?php echo $this->Form->input('filiere_id',array('label'=>''));?></td>
</tr>
<tr>

<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"etudiants","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>





