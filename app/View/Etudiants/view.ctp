<div class="etudiants view">
<h2><?php  echo __('Etudiant'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prenom'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['prenom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['adresse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cin'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['cin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sexe'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['sexe']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valider'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['valider']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($etudiant['Etudiant']['updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($etudiant['Filiere'][''], array('controller' => 'filieres', 'action' => 'view', $etudiant['Filiere']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Etudiant'), array('action' => 'edit', $etudiant['Etudiant']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Etudiant'), array('action' => 'delete', $etudiant['Etudiant']['id']), null, __('Are you sure you want to delete # %s?', $etudiant['Etudiant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudiants'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudiant'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Filieres'), array('controller' => 'filieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiere'), array('controller' => 'filieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($etudiant['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Etudiant Id'); ?></th>
		<th><?php echo __('Group-id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($etudiant['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['etudiant_id']; ?></td>
			<td><?php echo $user['group-id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
