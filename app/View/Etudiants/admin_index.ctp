<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">etudiants</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"etudiants","action"=>"add","admin"=>true));?>">Ajouter une nouvelle etudiant</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des etudiants</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Photo</th>
<th>Nom</th>
<th>Prenom</th>
<th>Adesse</th>
<th>Tel</th>
<th>Cne</th>
<th>Email</th>
<th>sexe</th>
<th>Filiere</th>
<th>actions</th>
</tr>
<?php foreach ($etudiants as $etudiant): ?>
	<tr>
		<td><?php echo h($etudiant['Etudiant']['id']); ?>&nbsp;</td>
		<td><?php echo $this->Html->image("Upload/".$etudiant["Etudiant"]["image"],array("height"=>"280","width"=>"100"));?></td>
        <td><?php echo h($etudiant['Etudiant']['nom']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['prenom']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['adresse']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['tel']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['cin']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['email']); ?>&nbsp;</td>
		<td><?php echo h($etudiant['Etudiant']['sexe']); ?>&nbsp;</td>
			<td><?php echo h($etudiant['Filiere']['libelle']); ?>&nbsp;</td>
			
			
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $etudiant['Etudiant']['id'])); ?>|
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $etudiant['Etudiant']['id']), null, __('Vous �tes s�r de vouloir supprimer element avec ID :# %s?', $etudiant['Etudiant']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>