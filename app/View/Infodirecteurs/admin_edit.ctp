<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Infodirecteurs</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Modifier</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Modifier une informations</h3>
						</div>
<?php echo $this->Form->create('Infodirecteur',array('type'=>'file')); ?>
<table cellpadding="20px">

<td>nom du directeur : </td>
<td>	<?php echo $this->Form->input('nom',array('label'=>''));?>
<?php echo $this->Form->input('id',array('label'=>''));?>
</td>
</tr>
<tr>
<td>prenom du directeur : </td>
<td>	<?php echo $this->Form->input('prenom',array('label'=>''));?></td>
</tr>
<tr>
<td>signe du directeur : </td>
<td>	<?php echo $this->Form->input('signe',array('label'=>''));?></td>
</tr>
<td>image du directeur : </td>
<td>	<?php echo $this->Form->input('imagefile',array('label'=>'','type'=>'file'));?></td>
</tr>
<td>mot du directeur : </td>
<td>	<?php echo $this->Form->input('mot_de_directeur',array('label'=>''));?></td>
</tr>
<tr>
<tr>
<tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Infodirecteurs","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>


				