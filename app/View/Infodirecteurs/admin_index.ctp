
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Infodirecteurs</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
			
<br />
<div class="span12">
<div class="widget-head blue">
							<h3></h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Nom</th>
<th>Prenom</th>
<th>image</th>
<th>signe</th>
<th>mot_du_directeur</th>
</tr>
<?php foreach ($infodirecteurs as $infodirecteur): ?>
	<tr>
		<td><?php echo h($infodirecteur['Infodirecteur']['id']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['nom']); ?>&nbsp;</td>
        	<td><?php echo h($infodirecteur['Infodirecteur']['prenom']); ?>&nbsp;</td>
            <td><?php echo h($infodirecteur['Infodirecteur']['image']); ?>&nbsp;</td>
            <td><?php echo h($infodirecteur['Infodirecteur']['signe']); ?>&nbsp;</td>
            <td><?php echo h($infodirecteur['Infodirecteur']['mot_de_directeur']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $infodirecteur['Infodirecteur']['id'])); ?>|
            <?php echo $this->Form->postLink(__('supprimer'), array('action' => 'delete', $infodirecteur['Infodirecteur']['id']), null, __('etes vous sur de vouloir supprimer cette information ? # %s?', $infodirecteur['Infodirecteur']['id'])); ?>
		
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>