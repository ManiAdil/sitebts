<div class="infodirecteurs view">
<h2><?php  echo __('Infodirecteur'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prenom'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['prenom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Signe'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['signe']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mot De Directeur'); ?></dt>
		<dd>
			<?php echo h($infodirecteur['Infodirecteur']['mot_de_directeur']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Infodirecteur'), array('action' => 'edit', $infodirecteur['Infodirecteur']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Infodirecteur'), array('action' => 'delete', $infodirecteur['Infodirecteur']['id']), null, __('Are you sure you want to delete # %s?', $infodirecteur['Infodirecteur']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Infodirecteurs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Infodirecteur'), array('action' => 'add')); ?> </li>
	</ul>
</div>
