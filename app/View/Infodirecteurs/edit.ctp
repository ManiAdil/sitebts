<div class="infodirecteurs form">
<?php echo $this->Form->create('Infodirecteur'); ?>
	<fieldset>
		<legend><?php echo __('Edit Infodirecteur'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nom');
		echo $this->Form->input('prenom');
		echo $this->Form->input('signe');
		echo $this->Form->input('image');
		echo $this->Form->input('mot_de_directeur');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Infodirecteur.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Infodirecteur.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Infodirecteurs'), array('action' => 'index')); ?></li>
	</ul>
</div>
