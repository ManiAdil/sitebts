<div class="infodirecteurs index">
	<h2><?php echo __('Infodirecteurs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th><?php echo $this->Paginator->sort('prenom'); ?></th>
			<th><?php echo $this->Paginator->sort('signe'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('mot_de_directeur'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($infodirecteurs as $infodirecteur): ?>
	<tr>
		<td><?php echo h($infodirecteur['Infodirecteur']['id']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['nom']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['prenom']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['signe']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['image']); ?>&nbsp;</td>
		<td><?php echo h($infodirecteur['Infodirecteur']['mot_de_directeur']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $infodirecteur['Infodirecteur']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $infodirecteur['Infodirecteur']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $infodirecteur['Infodirecteur']['id']), null, __('Are you sure you want to delete # %s?', $infodirecteur['Infodirecteur']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Infodirecteur'), array('action' => 'add')); ?></li>
	</ul>
</div>
