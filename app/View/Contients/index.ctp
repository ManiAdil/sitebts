<div class="contients index">
	<h2><?php echo __('Contients'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('module_id'); ?></th>
			<th><?php echo $this->Paginator->sort('matiere_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contients as $contient): ?>
	<tr>
		<td><?php echo h($contient['Contient']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($contient['Module']['nom'], array('controller' => 'modules', 'action' => 'view', $contient['Module']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($contient['Matiere']['id'], array('controller' => 'matieres', 'action' => 'view', $contient['Matiere']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contient['Contient']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contient['Contient']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contient['Contient']['id']), null, __('Are you sure you want to delete # %s?', $contient['Contient']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contient'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
	</ul>
</div>
