<div class="contients form">
<?php echo $this->Form->create('Contient'); ?>
	<fieldset>
		<legend><?php echo __('Edit Contient'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('module_id');
		echo $this->Form->input('matiere_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Contient.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Contient.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Contients'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
	</ul>
</div>
