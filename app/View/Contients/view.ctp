<div class="contients view">
<h2><?php  echo __('Contient'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contient['Contient']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Module'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contient['Module']['nom'], array('controller' => 'modules', 'action' => 'view', $contient['Module']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Matiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contient['Matiere']['id'], array('controller' => 'matieres', 'action' => 'view', $contient['Matiere']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contient'), array('action' => 'edit', $contient['Contient']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contient'), array('action' => 'delete', $contient['Contient']['id']), null, __('Are you sure you want to delete # %s?', $contient['Contient']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contients'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modules'), array('controller' => 'modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('controller' => 'modules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
	</ul>
</div>
