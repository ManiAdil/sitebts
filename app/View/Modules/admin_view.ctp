<div class="modules view">
<h2><?php  echo __('Module'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($module['Module']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($module['Module']['nom']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Module'), array('action' => 'edit', $module['Module']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Module'), array('action' => 'delete', $module['Module']['id']), null, __('Are you sure you want to delete # %s?', $module['Module']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Modules'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Module'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contients'), array('controller' => 'contients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('controller' => 'plusieurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Contients'); ?></h3>
	<?php if (!empty($module['Contient'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Module Id'); ?></th>
		<th><?php echo __('Matiere Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($module['Contient'] as $contient): ?>
		<tr>
			<td><?php echo $contient['id']; ?></td>
			<td><?php echo $contient['module_id']; ?></td>
			<td><?php echo $contient['matiere_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'contients', 'action' => 'view', $contient['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'contients', 'action' => 'edit', $contient['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'contients', 'action' => 'delete', $contient['id']), null, __('Are you sure you want to delete # %s?', $contient['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Plusieurs'); ?></h3>
	<?php if (!empty($module['Plusieur'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Module Id'); ?></th>
		<th><?php echo __('Unite Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($module['Plusieur'] as $plusieur): ?>
		<tr>
			<td><?php echo $plusieur['id']; ?></td>
			<td><?php echo $plusieur['module_id']; ?></td>
			<td><?php echo $plusieur['unite_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'plusieurs', 'action' => 'view', $plusieur['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'plusieurs', 'action' => 'edit', $plusieur['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'plusieurs', 'action' => 'delete', $plusieur['id']), null, __('Are you sure you want to delete # %s?', $plusieur['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
