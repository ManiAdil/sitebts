<div class="modules form">
<?php echo $this->Form->create('Module'); ?>
	<fieldset>
		<legend><?php echo __('Add Module'); ?></legend>
	<?php
		echo $this->Form->input('nom');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Modules'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Contients'), array('controller' => 'contients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contient'), array('controller' => 'contients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('controller' => 'plusieurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
