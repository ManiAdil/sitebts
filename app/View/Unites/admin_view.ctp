<div class="unites view">
<h2><?php  echo __('Unite'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($unite['Unite']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($unite['Unite']['nom']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Unite'), array('action' => 'edit', $unite['Unite']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Unite'), array('action' => 'delete', $unite['Unite']['id']), null, __('Are you sure you want to delete # %s?', $unite['Unite']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Unites'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Unite'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('controller' => 'plusieurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Plusieurs'); ?></h3>
	<?php if (!empty($unite['Plusieur'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Module Id'); ?></th>
		<th><?php echo __('Unite Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($unite['Plusieur'] as $plusieur): ?>
		<tr>
			<td><?php echo $plusieur['id']; ?></td>
			<td><?php echo $plusieur['module_id']; ?></td>
			<td><?php echo $plusieur['unite_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'plusieurs', 'action' => 'view', $plusieur['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'plusieurs', 'action' => 'edit', $plusieur['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'plusieurs', 'action' => 'delete', $plusieur['id']), null, __('Are you sure you want to delete # %s?', $plusieur['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
