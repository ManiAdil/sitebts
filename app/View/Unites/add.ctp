<div class="unites form">
<?php echo $this->Form->create('Unite'); ?>
	<fieldset>
		<legend><?php echo __('Add Unite'); ?></legend>
	<?php
		echo $this->Form->input('nom');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Unites'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('controller' => 'plusieurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
