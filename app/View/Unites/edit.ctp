<div class="unites form">
<?php echo $this->Form->create('Unite'); ?>
	<fieldset>
		<legend><?php echo __('Edit Unite'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nom');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Unite.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Unite.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Unites'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plusieurs'), array('controller' => 'plusieurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plusieur'), array('controller' => 'plusieurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
