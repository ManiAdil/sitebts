<div class="information view">
<h2><?php  echo __('Information'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($information['Information']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Presentation'); ?></dt>
		<dd>
			<?php echo h($information['Information']['presentation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Historique'); ?></dt>
		<dd>
			<?php echo h($information['Information']['historique']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reglement'); ?></dt>
		<dd>
			<?php echo h($information['Information']['reglement']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Information'), array('action' => 'edit', $information['Information']['id'])); ?> </li>
		
		<li><?php echo $this->Html->link(__('List Information'), array('action' => 'index')); ?> </li>

	</ul>
</div>
