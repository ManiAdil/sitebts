
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Informations</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Modifier</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Modifier une information</h3>
						</div>
<?php echo $this->Form->create('Information'); ?>
<table cellpadding="20px">
<tr><td colspan="2">
<?php echo $this->Form->input('id',array('label'=>''));?>
</td></tr>
<tr>
<td>présentation : </td>
<td>	<?php echo $this->Form->input('presentation',array('label'=>''));?></td>
</tr>
<tr>
<td>Historique: </td>
<td>	<?php echo $this->Form->input('historique',array('label'=>''));?></td>
</tr>
<tr>
<td>Régelement : </td>
<td>	<?php echo $this->Form->input('regelement',array('label'=>''));?></td>
</tr>
<tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Informations","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>

