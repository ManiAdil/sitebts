<div class="information view">
<h2><?php  echo __('information'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($information['information']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('presentation'); ?></dt>
		<dd>
			<?php echo h($information['information']['presentation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('historique'); ?></dt>
		<dd>
			<?php echo h($information['information']['historique']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('regelement'); ?></dt>
		<dd>
			<?php echo h($information['information']['regelement']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit information'), array('action' => 'edit', $information['information']['id'])); ?> </li>
		
		<li><?php echo $this->Html->link(__('List information'), array('action' => 'index')); ?> </li>
	
	</ul>
</div>