
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Informations</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
			
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des informations</h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>presentation</th>
<th>historique</th>
<th>reglement</th>

<th>Actions</th>
</tr>
<?php foreach ($information as $information): ?>
	<tr>
		<td><?php echo h($information['Information']['id']); ?>&nbsp;</td>
        <td><?php echo h($information['Information']['presentation']); ?>&nbsp;</td>
		<td><?php echo h($information['Information']['historique']); ?>&nbsp;</td>
	    <td><?php echo h($information['Information']['reglement']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $information['Information']['id'])); ?>|

		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>