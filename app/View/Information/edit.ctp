<div class="information form">
<?php echo $this->Form->create('Information'); ?>
	<fieldset>
		<legend><?php echo __('Edit Information'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('presentation');
		echo $this->Form->input('historique');
		echo $this->Form->input('reglement');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

	
		<li><?php echo $this->Html->link(__('List Information'), array('action' => 'index')); ?></li>
	</ul>
</div>
