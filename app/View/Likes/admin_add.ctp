<div class="likes form">
<?php echo $this->Form->create('Like'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Like'); ?></legend>
	<?php
		echo $this->Form->input('reponse_id');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Likes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Reponses'), array('controller' => 'reponses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reponse'), array('controller' => 'reponses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
