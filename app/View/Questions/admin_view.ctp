<div class="questions view">
<h2><?php  echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('nom'); ?></dt>
		<dd>
			<?php echo h($question['Question']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('email'); ?></dt>
		<dd>
			<?php echo h($question['Question']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('contenu'); ?></dt>
		<dd>
			<?php echo h($question['Question']['contenu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('reselu'); ?></dt>
		<dd>
			<?php echo h($question['Question']['resolu']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), null, __('Are you sure you want to delete # %s?', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Question'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
	</ul>
</div>
