
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Question</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"question","action"=>"add","admin"=>true));?>">Ajouter une question</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste de  mes questions </h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>

<th>nom</th>
<th>contenu</th>

<th>Afficher réponses</th>
</tr>

<?php foreach ($mesquestions as $question): ?>
	<tr>
		<?php if ($question['Question']['resolu']==0){?>
        	<td style="background-color: #ea758a;"><?php echo h($question['Question']['nom']); ?>&nbsp;</td>
        <?php }else{?>
        	<td style="background-color: #52e488;"><?php echo h($question['Question']['nom']); ?>&nbsp;</td>
        <?php }?>
        
        <?php if ($question['Question']['resolu']==0){?>
        	<td style="background-color: #ea758a;"><?php echo h($question['Question']['contenu']); ?>&nbsp;</td>
        <?php }else{?>
        	<td style="background-color: #52e488;"><?php echo h($question['Question']['contenu']); ?>&nbsp;</td>
        <?php }?>
	<?php if ($question['Question']['resolu']== 0){?>
		
		<td style="background-color: #ea758a;" >
			
					<?php echo $this->Html->link(__('marquer résolu'), array('action' => 'marquer_resolu', $question['Question']['id'])); ?>
		
        
        
      |
	
			
					  <?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $question['Question']['nom']), null, __('Vous ète sur de supprimer cette question de  nom %s?', $question['Question']['nom'])); ?>
		</td>
        
<?php }else{?>
        
        <td style="background-color: #52e488;" >
			
					<?php   echo $this->Html->link(__('marquer résolu'), array('action' => 'marquer_resolu', $question['Question']['id'])); ?>
		
        
        
      |
	
			
                    
                    
                    
                    <?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $question['Question']['nom']), null, __('Vous ète sur de supprimer cette question  de nom : %s ?', $question['Question']['nom'])); ?>
                    
                    
                    
                    
                    
		</td>
 <?php }?>
</tr>
<?php endforeach; ?>
							</tbody>
							
</table>





</div>
</div>