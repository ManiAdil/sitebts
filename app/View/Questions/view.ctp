<div class="questions view">
<h2><?php  echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($question['Question']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($question['Question']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contenu'); ?></dt>
		<dd>
			<?php echo h($question['Question']['contenu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resolu'); ?></dt>
		<dd>
			<?php echo h($question['Question']['resolu']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($question['Question']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($question['Question']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), null, __('Are you sure you want to delete # %s?', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reponses'), array('controller' => 'reponses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reponse'), array('controller' => 'reponses', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Reponses'); ?></h3>
	<?php if (!empty($question['Reponse'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Contenu'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($question['Reponse'] as $reponse): ?>
		<tr>
			<td><?php echo $reponse['id']; ?></td>
			<td><?php echo $reponse['contenu']; ?></td>
			<td><?php echo $reponse['question_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'reponses', 'action' => 'view', $reponse['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'reponses', 'action' => 'edit', $reponse['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'reponses', 'action' => 'delete', $reponse['id']), null, __('Are you sure you want to delete # %s?', $reponse['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reponse'), array('controller' => 'reponses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
