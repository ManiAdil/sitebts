
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Question</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>Liste des questions non validées </h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>

<th>nom</th>
<th>contenu</th>
<th>Valider des questions </th>

</tr>

<?php foreach ($questions as $question):
if ($question["Question"]["valider"] == 0){ ?>
	<tr>
	
        
   
        	<td><?php echo h($question['Question']['nom']); ?>&nbsp;</td>
	<td><?php echo h($question['Question']['contenu']); ?>&nbsp;</td>
		<td>
			
					<?php echo $this->Html->link(__('Valider la question'), array('action' => 'marquer_valide', $question['Question']['id'])); ?>
			</td>
</tr>
<?php } endforeach; ?>
</tbody>
</table>
</div>
</div>