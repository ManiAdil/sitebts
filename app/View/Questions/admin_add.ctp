
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Question</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Envoyé</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Envoyé une Question</h3>
						</div>
<?php echo $this->Form->create('Question'); ?>
<table cellpadding="20px">
<tr>
<td>Nom : <?php echo $this->Session->read("complet");?>	<?php echo $this->Form->input('nom',array('label'=>'','type'=>'hidden','value'=>$this->Session->read("complet")));?></td>
</td>
<td>	<?php echo $this->Form->input('user_id',array('label'=>'','type'=>'hidden','value'=>$this->Session->read('id')));?></td>
</tr>
<tr>
<td>Email : <?php echo $this->Session->read("email");?></td>
<td>	<?php echo $this->Form->input('email',array('label'=>'','type'=>'hidden','value'=>$this->Session->read('email')));?></td>
</tr>
<tr>
<td>Contenu : </td>
<td>	<?php echo $this->Form->input('contenu',array('label'=>''));?></td>
</tr>

<tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Questions","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>

