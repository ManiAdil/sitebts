
				<div class="span12">
					<div class="primary-head">
					
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">liste des affectations</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Liste</li>
					</ul>
				</div>
		
			<div class="row-fluid">
<center><a class="btn btn-primary" href="<?php echo $this->Html->url(array("controller"=>"Affectations","action"=>"add","admin"=>true));?>">Ajouter une nouvelle affectation</a></center>				
<br />
<div class="span12">
<div class="widget-head blue">
							<h3>liste des </h3>
</div>
<table class="stat-table table table-stats table-striped table-sortable table-bordered">
							<tbody>
<tr>
<th>#</th>
<th>Classe</th>
<th>Enseignant</th>
<th>Date Debut</th>
<th>Date Fin</th>
<th>Action</th>
</tr>
<?php foreach ($affectations as $affectation): ?>
	<tr>
		<td><?php echo h($affectation['Affectation']['id']); ?>&nbsp;</td>
        <td><?php echo h($affectation['Classee']['nom']); ?>&nbsp;</td>
        <td><?php echo h($affectation['Enseignant']['nom']); ?>&nbsp;</td>
        <td><?php echo h($affectation['Affectation']['datedebut']); ?>&nbsp;</td>
        <td><?php echo h($affectation['Affectation']['datefin']); ?>&nbsp;</td>
		<td class="actions">
	
			<?php echo $this->Html->link(__('Modifier'), array('action' => 'edit', $affectation['Affectation']['id'])); ?>|
            	<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $affectation['Affectation']['id']), null, __('Are you sure you want to delete # %s?', $affectation['Affectation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							
</table>

</div>
</div>