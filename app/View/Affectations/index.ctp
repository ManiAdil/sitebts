<div class="affectations index">
	<h2><?php echo __('Affectations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('classee_id'); ?></th>
			<th><?php echo $this->Paginator->sort('enseignant_id'); ?></th>
			<th><?php echo $this->Paginator->sort('date debut'); ?></th>
			<th><?php echo $this->Paginator->sort('date fin'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($affectations as $affectation): ?>
	<tr>
		<td><?php echo h($affectation['Affectation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($affectation['Classee']['id'], array('controller' => 'classees', 'action' => 'view', $affectation['Classee']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($affectation['Enseignant']['id'], array('controller' => 'enseignants', 'action' => 'view', $affectation['Enseignant']['id'])); ?>
		</td>
		<td><?php echo h($affectation['Affectation']['date debut']); ?>&nbsp;</td>
		<td><?php echo h($affectation['Affectation']['date fin']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $affectation['Affectation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $affectation['Affectation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $affectation['Affectation']['id']), null, __('Are you sure you want to delete # %s?', $affectation['Affectation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Affectation'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
	</ul>
</div>
