<div class="affectations view">
<h2><?php  echo __('Affectation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($affectation['Affectation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classee'); ?></dt>
		<dd>
			<?php echo $this->Html->link($affectation['Classee']['id'], array('controller' => 'classees', 'action' => 'view', $affectation['Classee']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enseignant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($affectation['Enseignant']['id'], array('controller' => 'enseignants', 'action' => 'view', $affectation['Enseignant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Debut'); ?></dt>
		<dd>
			<?php echo h($affectation['Affectation']['date debut']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Fin'); ?></dt>
		<dd>
			<?php echo h($affectation['Affectation']['date fin']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Affectation'), array('action' => 'edit', $affectation['Affectation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Affectation'), array('action' => 'delete', $affectation['Affectation']['id']), null, __('Are you sure you want to delete # %s?', $affectation['Affectation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Affectations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Affectation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Classees'), array('controller' => 'classees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classee'), array('controller' => 'classees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
	</ul>
</div>
