
				<div class="span12">
					<div class="primary-head">
				
						<ul class="top-right-toolbar">
							<li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" data-original-title="Users">
<i class="icon-user"></i></a></li>
							<li><a href="#" class="green" data-original-title="Upload"><i class=" icon-upload-alt"></i></a></li>
							<li><a href="#" class="bondi-blue" data-original-title="Settings"><i class="icon-cogs"></i></a></li>
						</ul>
					</div>
					<ul class="breadcrumb">
						<li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right"></i></span></li>

						<li><a href="#">Absence</a><span class="divider"><i class="icon-angle-right"></i></span></li>

						<li class="active">Ajouter</li>
					</ul>
				</div>
		
			<div class="row-fluid">
				
<div class="span12">
<div class="widget-head blue">
							<h3>Ajouter une absence</h3>
						</div>
<?php echo $this->Form->create('Absence',array("type"=>"file")); ?>
<table cellpadding="20px">
<tr>
<td>Heure: </td>
<td>	<?php echo $this->Form->time('heure',array('label'=>''));?></td>
</tr>
<tr>
<td>Heure FIN: </td>
<td>	<?php echo $this->Form->time('heurefin',array('label'=>''));?></td>
</tr>
<tr>
<td>Jour: </td>
<td>	<?php echo $this->Form->date('jour', array('dateFormat' => 'DMY', 'label' => ''));?></td>
</tr>
<tr>
<td>Enseignant: </td>
<td>	<?php echo $this->Form->input('enseignant_id',array('label'=>''));?></td>
</tr>
<tr>
<td>Matiere: </td>
<td>	<?php echo $this->Form->input('matiere_id',array('label'=>''));?></td>
</tr>
<td>Etudiant: </td>
<td>	<?php echo $this->Form->input('etudiant_id',array('label'=>''));?></td>
</tr>
<td colspan="2"><input type="submit" value="Valider l'enregistrement" class="btn btn-primary"/>
<a class="btn" href="<?php echo $this->Html->url(array("controller"=>"Absences","action"=>"index","admin"=>true));?>">Annuler</a>
</td>
</tr>
</table>	
</div>
</div>

