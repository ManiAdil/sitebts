<div class="absences index">
	<h2><?php echo __('Absences'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('heure'); ?></th>
			<th><?php echo $this->Paginator->sort('heurefin'); ?></th>
			<th><?php echo $this->Paginator->sort('jour'); ?></th>
			<th><?php echo $this->Paginator->sort('enseignant_id'); ?></th>
			<th><?php echo $this->Paginator->sort('matiere_id'); ?></th>
			<th><?php echo $this->Paginator->sort('etudiant_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($absences as $absence): ?>
	<tr>
		<td><?php echo h($absence['Absence']['id']); ?>&nbsp;</td>
		<td><?php echo h($absence['Absence']['heure']); ?>&nbsp;</td>
		<td><?php echo h($absence['Absence']['heurefin']); ?>&nbsp;</td>
		<td><?php echo h($absence['Absence']['jour']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($absence['Enseignant']['nom'], array('controller' => 'enseignants', 'action' => 'view', $absence['Enseignant']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($absence['Matiere']['nom'], array('controller' => 'matieres', 'action' => 'view', $absence['Matiere']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($absence['Etudiant']['id'], array('controller' => 'etudiants', 'action' => 'view', $absence['Etudiant']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $absence['Absence']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $absence['Absence']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $absence['Absence']['id']), null, __('Are you sure you want to delete # %s?', $absence['Absence']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Absence'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudiants'), array('controller' => 'etudiants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudiant'), array('controller' => 'etudiants', 'action' => 'add')); ?> </li>
	</ul>
</div>
