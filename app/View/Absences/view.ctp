<div class="absences view">
<h2><?php  echo __('Absence'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($absence['Absence']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Heure'); ?></dt>
		<dd>
			<?php echo h($absence['Absence']['heure']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Heurefin'); ?></dt>
		<dd>
			<?php echo h($absence['Absence']['heurefin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jour'); ?></dt>
		<dd>
			<?php echo h($absence['Absence']['jour']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enseignant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($absence['Enseignant']['nom'], array('controller' => 'enseignants', 'action' => 'view', $absence['Enseignant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Matiere'); ?></dt>
		<dd>
			<?php echo $this->Html->link($absence['Matiere']['nom'], array('controller' => 'matieres', 'action' => 'view', $absence['Matiere']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Etudiant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($absence['Etudiant']['id'], array('controller' => 'etudiants', 'action' => 'view', $absence['Etudiant']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Absence'), array('action' => 'edit', $absence['Absence']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Absence'), array('action' => 'delete', $absence['Absence']['id']), null, __('Are you sure you want to delete # %s?', $absence['Absence']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Absences'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Absence'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Enseignants'), array('controller' => 'enseignants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Enseignant'), array('controller' => 'enseignants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Matieres'), array('controller' => 'matieres', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Matiere'), array('controller' => 'matieres', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Etudiants'), array('controller' => 'etudiants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Etudiant'), array('controller' => 'etudiants', 'action' => 'add')); ?> </li>
	</ul>
</div>
