<article class="grid_6 omega">
<h5 style="text-transform: uppercase; text-decoration: underline;text-align: center;">Mot de Directeur :</h5>
<h6 style="text-transform: uppercase; text-decoration: underline;text-align: center;">
<?php echo $widget["Infodirecteur"]["Infodirecteur"]["signe"].".".$widget["Infodirecteur"]["Infodirecteur"]["nom"]." ".$widget["Infodirecteur"]["Infodirecteur"]["prenom"]?>
 :</h6>
<ul class="list-2">
<li>
<p class="p0"><?php echo $widget["Infodirecteur"]["Infodirecteur"]["mot_de_directeur"];?><br /></p>
</li>
</ul>

<h5 style="text-transform: uppercase; text-decoration: underline;text-align: center;">Actualités :</h5>
<ul class="list-2">
<?php foreach($widget["News"] as $new):?>
<li>
<p class="p0"><h6><?php echo $new["News"]["titre"]?></h6></p>
</li>
<?php endforeach;?>
</ul>
<h5 style="text-transform: uppercase; text-decoration: underline;text-align: center;">Sites Amis :</h5>
<ul class="list-2"><?php foreach($widget["Sitesamis"] as $new):?>
<li>
<p class="p0"><h6><a target="new" href="<?php echo $new["Sitesami"]["URL"];?>"><?php echo $new["Sitesami"]["libelle"]?></a></h6></p>
</li>
<?php endforeach;?>
</ul>
</article>