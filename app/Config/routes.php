<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/presentationdip', array('controller' => 'pages', 'action' => 'presentationdiplom'));
	Router::connect('/conditionacces', array('controller' => 'pages', 'action' => 'conditionacces'));
    Router::connect('/admin', array('controller' => 'pages', 'action' => 'admin'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
    	Router::connect('/contactez-nous', array('controller' => 'pages', 'action' => 'contact'));
    	Router::connect('/nos-coordonees', array('controller' => 'pages', 'action' => 'nos_coordones'));
        	Router::connect('/etineraire', array('controller' => 'pages', 'action' => 'etineraire'));
        Router::connect('/notre-localisation', array('controller' => 'pages', 'action' => 'localisation'));
	    Router::connect('/presentation-bst', array('controller' => 'pages', 'action' => 'presentation_bts'));
		Router::connect('/reglement', array('controller' => 'pages', 'action' => 'Reglement'));
        Router::connect('/annonces', array('controller' => 'pages', 'action' => 'annonces'));

 Router::connect('/news', array('controller' => 'pages', 'action' => 'news'));
  Router::connect('/information-directeurs', array('controller' => 'pages', 'action' => 'information-directeurs'));

	Router::connect('/nos-missions', array('controller' => 'pages', 'action' => 'missions'));
    Router::connect('/nos-infos', array('controller' => 'pages', 'action' => 'informations'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
